#pragma once
#ifndef STEAM_POWER_H
#define STEAM_POWER_H
#define _CRT_SECURE_NO_WARNINGS

#include "common.h"

	class SteamPowerEvent {
	public:
		string type;
		string error;
		int    Result_code;
		SteamPowerEvent(const char * s) : type("Undefined"), error("Success"), Result_code(1) { type = s; };
		virtual bool tolua(lua_State * L) {
			lua_pushnumber(L, Result_code);
			luax_pushString(L, type);
			if (Result_code != 1) luax_pushString(L, error);
			return (Result_code == 1);
		};
	};

	// tolua() using lua-table on top of stack
	struct SteamPower_e_leaderboard_rank {
		string SteamID;
		int    Score;
		int    GlobalRank;

		SteamPower_e_leaderboard_rank(LeaderboardEntry_t * entry) {
			SteamID = uint64ToString(entry->m_steamIDUser.GetAccountID());
			Score   = entry->m_nScore;
			GlobalRank = entry->m_nGlobalRank;
		};
		void tolua(lua_State * L) {
			luax_tstring(L, "IDUser", SteamID.c_str());
			luax_tnumber(L, "Score", Score);
			luax_tnumber(L, "GlobalRank", GlobalRank);
		};
	};


	#define event std::shared_ptr<SteamPowerEvent>

	// oversize protection
	#define MAX_EVENT_COUNT 100

	/* // debug info class and hook, currently buggy
	#define MAX_WARNING_COUNT 100

	class warning_message {
	public:
		bool        Type;
		const char *Text;
		warning_message(int type, const char *text) {
			Type = type;
			Text = text;
		};
		void tolua(lua_State * L) {
			lua_newtable(L);
			if (Type == 0){
				luax_tstring(L, "Type", "Message");
			}
			else if (Type == 1){
				luax_tstring(L, "Type", "Warning");
			}
			luax_tstring(L, "Text", Text);
		};
	};

	std::vector<warning_message> warning_list;

	extern "C" void __cdecl warning_hook(int nSeverity, const char *pchDebugText) {
		if (warning_list.size() < MAX_WARNING_COUNT)
			warning_list.push_back(warning_message(nSeverity, pchDebugText));
	};

	SteamUtils()->SetWarningMessageHook(&warning_hook);
	*/



	// Main initialisator and callback-maschine
	class Steam {
	public:
		bool initialized = false;

		// bool debugmode on/off
		int init(bool v);

		// indicates activity of debug
		bool _dbg = false;

		// prints in std output `date time [Steam lib]: "formatted message" \n`
		void debug(const char* fmt, ...);

		// bool on/off debug output
		void setDebug(bool v) { _dbg = v; }

		// callback-events, steam_event.h/steam_event.cpp
		std::vector<event> event_list;

		// deprecated now, used directly for child-methods calling
		event _event_poll();

		// push shared pointer to event list, while events count less then [MAX_EVENT_COUNT]
		void  _event_push(event e);

		// clear event list
		void  _event_clear();

		// update events
		int   _event_flush();

		// returns count of events in list
		int   _event_count();

		SteamLeaderboard_t leaderboard_handle = {};
		std::vector<SteamPower_e_leaderboard_rank>  leaderboard_entries = {};

		bool getDownloadedLeaderboardEntry(SteamLeaderboardEntries_t eHandle, int entryCount);

		//// AppList
		  STEAM_CALLBACK(Steam, _SteamAppInstalled_t,                     SteamAppInstalled_t);
		  STEAM_CALLBACK(Steam, _SteamAppUninstalled_t,                   SteamAppUninstalled_t);
		//
		//// Apps
		//STEAM_CALLBACK(Steam, _AppProofOfPurchaseKeyResponse_t,         AppProofOfPurchaseKeyResponse_t); // internaly on steam
		  STEAM_CALLBACK(Steam, _DlcInstalled_t,                          DlcInstalled_t);
		  STEAM_CALLBACK(Steam, _FileDetailsResult_t,                     FileDetailsResult_t);
		//
		// Friends
		  STEAM_CALLBACK(Steam, _AvatarImageLoaded_t,                     AvatarImageLoaded_t);
		  STEAM_CALLBACK(Steam, _ClanOfficerListResponse_t,               ClanOfficerListResponse_t);
		  STEAM_CALLBACK(Steam, _DownloadClanActivityCountsResult_t,      DownloadClanActivityCountsResult_t);
		  STEAM_CALLBACK(Steam, _FriendRichPresenceUpdate_t,              FriendRichPresenceUpdate_t);
		  STEAM_CALLBACK(Steam, _FriendsEnumerateFollowingList_t,         FriendsEnumerateFollowingList_t);
		  STEAM_CALLBACK(Steam, _FriendsGetFollowerCount_t,               FriendsGetFollowerCount_t);
		  STEAM_CALLBACK(Steam, _FriendsIsFollowing_t,                    FriendsIsFollowing_t);
		  STEAM_CALLBACK(Steam, _GameConnectedChatJoin_t,                 GameConnectedChatJoin_t);
		  STEAM_CALLBACK(Steam, _GameConnectedChatLeave_t,                GameConnectedChatLeave_t);
		  STEAM_CALLBACK(Steam, _GameConnectedClanChatMsg_t,              GameConnectedClanChatMsg_t);
		  STEAM_CALLBACK(Steam, _GameConnectedFriendChatMsg_t,            GameConnectedFriendChatMsg_t);
		  STEAM_CALLBACK(Steam, _GameLobbyJoinRequested_t,                GameLobbyJoinRequested_t);
		  STEAM_CALLBACK(Steam, _GameOverlayActivated_t,                  GameOverlayActivated_t);
		  STEAM_CALLBACK(Steam, _GameRichPresenceJoinRequested_t,         GameRichPresenceJoinRequested_t);
		  STEAM_CALLBACK(Steam, _GameServerChangeRequested_t,             GameServerChangeRequested_t);
		  STEAM_CALLBACK(Steam, _JoinClanChatRoomCompletionResult_t,      JoinClanChatRoomCompletionResult_t);
		  STEAM_CALLBACK(Steam, _PersonaStateChange_t,                    PersonaStateChange_t);
		  STEAM_CALLBACK(Steam, _SetPersonaNameResponse_t,                SetPersonaNameResponse_t);
		//
		// HTMLSurface
		  STEAM_CALLBACK(Steam, _HTML_BrowserReady_t,                     HTML_BrowserReady_t);
		  STEAM_CALLBACK(Steam, _HTML_CanGoBackAndForward_t,              HTML_CanGoBackAndForward_t);
		  STEAM_CALLBACK(Steam, _HTML_ChangedTitle_t,                     HTML_ChangedTitle_t);
		  STEAM_CALLBACK(Steam, _HTML_CloseBrowser_t,                     HTML_CloseBrowser_t);
		  STEAM_CALLBACK(Steam, _HTML_FileOpenDialog_t,                   HTML_FileOpenDialog_t);
		  STEAM_CALLBACK(Steam, _HTML_FinishedRequest_t,                  HTML_FinishedRequest_t);
		  STEAM_CALLBACK(Steam, _HTML_HideToolTip_t,                      HTML_HideToolTip_t);
		  STEAM_CALLBACK(Steam, _HTML_HorizontalScroll_t,                 HTML_HorizontalScroll_t);
		  STEAM_CALLBACK(Steam, _HTML_JSAlert_t,                          HTML_JSAlert_t);
		  STEAM_CALLBACK(Steam, _HTML_JSConfirm_t,                        HTML_JSConfirm_t);
		  STEAM_CALLBACK(Steam, _HTML_LinkAtPosition_t,                   HTML_LinkAtPosition_t);
		  STEAM_CALLBACK(Steam, _HTML_NeedsPaint_t,                       HTML_NeedsPaint_t);
		  STEAM_CALLBACK(Steam, _HTML_NewWindow_t,                        HTML_NewWindow_t);
		  STEAM_CALLBACK(Steam, _HTML_OpenLinkInNewTab_t,                 HTML_OpenLinkInNewTab_t);
		  STEAM_CALLBACK(Steam, _HTML_SearchResults_t,                    HTML_SearchResults_t);
		  STEAM_CALLBACK(Steam, _HTML_SetCursor_t,                        HTML_SetCursor_t);
		  STEAM_CALLBACK(Steam, _HTML_ShowToolTip_t,                      HTML_ShowToolTip_t);
		  STEAM_CALLBACK(Steam, _HTML_StartRequest_t,                     HTML_StartRequest_t);
		  STEAM_CALLBACK(Steam, _HTML_StatusText_t,                       HTML_StatusText_t);
		  STEAM_CALLBACK(Steam, _HTML_UpdateToolTip_t,                    HTML_UpdateToolTip_t);
		  STEAM_CALLBACK(Steam, _HTML_URLChanged_t,                       HTML_URLChanged_t);
		  STEAM_CALLBACK(Steam, _HTML_VerticalScroll_t,                   HTML_VerticalScroll_t);
		//
		//// HTTP
		//STEAM_CALLBACK(Steam, _HTTPRequestCompleted_t,                  HTTPRequestCompleted_t);
		//STEAM_CALLBACK(Steam, _HTTPRequestDataReceived_t,               HTTPRequestDataReceived_t);
		//STEAM_CALLBACK(Steam, _HTTPRequestHeadersReceived_t,            HTTPRequestHeadersReceived_t);
		//
		//// Inventory
		//STEAM_CALLBACK(Steam, _SteamInventoryDefinitionUpdate_t,        SteamInventoryDefinitionUpdate_t);
		//STEAM_CALLBACK(Steam, _SteamInventoryEligiblePromoItemDefIDs_t, SteamInventoryEligiblePromoItemDefIDs_t);
		//STEAM_CALLBACK(Steam, _SteamInventoryFullUpdate_t,              SteamInventoryFullUpdate_t);
		//STEAM_CALLBACK(Steam, _SteamInventoryResultReady_t,             SteamInventoryResultReady_t);
		//
		//// Matchmaking
		//STEAM_CALLBACK(Steam, _FavoritesListAccountsUpdated_t,          FavoritesListAccountsUpdated_t);
		//STEAM_CALLBACK(Steam, _FavoritesListChanged_t,                  FavoritesListChanged_t);
		//STEAM_CALLBACK(Steam, _LobbyChatMsg_t,                          LobbyChatMsg_t);
		//STEAM_CALLBACK(Steam, _LobbyChatUpdate_t,                       LobbyChatUpdate_t);
		  STEAM_CALLBACK(Steam, _LobbyCreated_t,                          LobbyCreated_t);
		//STEAM_CALLBACK(Steam, _LobbyDataUpdate_t,                       LobbyDataUpdate_t);
		//STEAM_CALLBACK(Steam, _LobbyEnter_t,                            LobbyEnter_t);
		  STEAM_CALLBACK(Steam, _LobbyInvite_t,                           LobbyInvite_t);
		//STEAM_CALLBACK(Steam, _LobbyGameCreated_t,                      LobbyGameCreated_t);
		//STEAM_CALLBACK(Steam, _LobbyKicked_t,                           LobbyKicked_t);
		//STEAM_CALLBACK(Steam, _LobbyMatchList_t,                        LobbyMatchList_t);
		//STEAM_CALLBACK(Steam, _PSNGameBootInviteResult_t,               PSNGameBootInviteResult_t);
		//
		//// Music
		//STEAM_CALLBACK(Steam, _PlaybackStatusHasChanged_t,              PlaybackStatusHasChanged_t);
		//STEAM_CALLBACK(Steam, _VolumeHasChanged_t,                      VolumeHasChanged_t);
		//
		//// MusicRemote
		//STEAM_CALLBACK(Steam, _MusicPlayerRemoteToFront_t,              MusicPlayerRemoteToFront_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerRemoteWillActivate_t,         MusicPlayerRemoteWillActivate_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerRemoteWillDeactivate_t,       MusicPlayerRemoteWillDeactivate_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerSelectsPlaylistEntry_t,       MusicPlayerSelectsPlaylistEntry_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerSelectsQueueEntry_t,          MusicPlayerSelectsQueueEntry_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsLooped_t,                MusicPlayerWantsLooped_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsPause_t,                 MusicPlayerWantsPause_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsPlayingRepeatStatus_t,   MusicPlayerWantsPlayingRepeatStatus_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsPlayNext_t,              MusicPlayerWantsPlayNext_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsPlayPrevious_t,          MusicPlayerWantsPlayPrevious_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsPlay_t,                  MusicPlayerWantsPlay_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsShuffled_t,              MusicPlayerWantsShuffled_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWantsVolume_t,                MusicPlayerWantsVolume_t);
		//STEAM_CALLBACK(Steam, _MusicPlayerWillQuit_t,                   MusicPlayerWillQuit_t);
		//
		//// Networking
		//STEAM_CALLBACK(Steam, _P2PSessionConnectFail_t,                 P2PSessionConnectFail_t);
		//STEAM_CALLBACK(Steam, _P2PSessionRequest_t,                     P2PSessionRequest_t);
		//STEAM_CALLBACK(Steam, _SocketStatusCallback_t,                  SocketStatusCallback_t);
		//
		//// RemoteStorage
		//STEAM_CALLBACK(Steam, _RemoteStorageDownloadUGCResult_t,        RemoteStorageDownloadUGCResult_t);
		//STEAM_CALLBACK(Steam, _RemoteStorageFileShareResult_t,          RemoteStorageFileShareResult_t);
		//STEAM_CALLBACK(Steam, _RemoteStorageFileWriteAsyncComplete_t,   RemoteStorageFileWriteAsyncComplete_t);
		//
		//// Screenshots
		//STEAM_CALLBACK(Steam, _ScreenshotReady_t,                       ScreenshotReady_t);
		//STEAM_CALLBACK(Steam, _ScreenshotRequested_t,                   ScreenshotRequested_t);
		//
		//// UGC
		//STEAM_CALLBACK(Steam, _AddAppDependencyResult_t,                AddAppDependencyResult_t);
		//STEAM_CALLBACK(Steam, _AddUGCDependencyResult_t,                AddUGCDependencyResult_t);
		//STEAM_CALLBACK(Steam, _CreateItemResult_t,                      CreateItemResult_t);
		//STEAM_CALLBACK(Steam, _DownloadItemResult_t,                    DownloadItemResult_t);
		//STEAM_CALLBACK(Steam, _GetAppDependenciesResult_t,              GetAppDependenciesResult_t);
		//STEAM_CALLBACK(Steam, _GetUserItemVoteResult_t,                 GetUserItemVoteResult_t);
		//STEAM_CALLBACK(Steam, _ItemInstalled_t,                         ItemInstalled_t);
		//STEAM_CALLBACK(Steam, _RemoveAppDependencyResult_t,             RemoveAppDependencyResult_t);
		//STEAM_CALLBACK(Steam, _RemoveUGCDependencyResult_t,             RemoveUGCDependencyResult_t);
		//STEAM_CALLBACK(Steam, _SetUserItemVoteResult_t,                 SetUserItemVoteResult_t);
		//STEAM_CALLBACK(Steam, _StartPlaytimeTrackingResult_t,           StartPlaytimeTrackingResult_t);
		//STEAM_CALLBACK(Steam, _SteamUGCQueryCompleted_t,                SteamUGCQueryCompleted_t);
		//STEAM_CALLBACK(Steam, _StopPlaytimeTrackingResult_t,            StopPlaytimeTrackingResult_t);
		//STEAM_CALLBACK(Steam, _SubmitItemUpdateResult_t,                SubmitItemUpdateResult_t);
		//STEAM_CALLBACK(Steam, _UserFavoriteItemsListChanged_t,          UserFavoriteItemsListChanged_t);
		//
		//// UnifiedMessages
		//STEAM_CALLBACK(Steam, _SteamUnifiedMessagesSendMethodResult_t,  SteamUnifiedMessagesSendMethodResult_t);
		//
		//// SteamUser
		//STEAM_CALLBACK(Steam, _ClientGameServerDeny_t,                  ClientGameServerDeny_t);
		//STEAM_CALLBACK(Steam, _EncryptedAppTicketResponse_t,            EncryptedAppTicketResponse_t);
		//STEAM_CALLBACK(Steam, _GameWebCallback_t,                       GameWebCallback_t);
		//STEAM_CALLBACK(Steam, _GetAuthSessionTicketResponse_t,          GetAuthSessionTicketResponse_t);
		//STEAM_CALLBACK(Steam, _IPCFailure_t,                            IPCFailure_t);
		//STEAM_CALLBACK(Steam, _LicensesUpdated_t,                       LicensesUpdated_t);
		//STEAM_CALLBACK(Steam, _MicroTxnAuthorizationResponse_t,         MicroTxnAuthorizationResponse_t);
		//STEAM_CALLBACK(Steam, _SteamServerConnectFailure_t,             SteamServerConnectFailure_t);
		  STEAM_CALLBACK(Steam, _SteamServersConnected_t,                 SteamServersConnected_t);
		  STEAM_CALLBACK(Steam, _SteamServersDisconnected_t,              SteamServersDisconnected_t);
		//STEAM_CALLBACK(Steam, _StoreAuthURLResponse_t,                  StoreAuthURLResponse_t);
		//STEAM_CALLBACK(Steam, _ValidateAuthTicketResponse_t,            ValidateAuthTicketResponse_t);
		//
		//// SteamUserStats
		  STEAM_CALLBACK(Steam, _GlobalAchievementPercentagesReady_t,     GlobalAchievementPercentagesReady_t);
		  STEAM_CALLBACK(Steam, _GlobalStatsReceived_t,                   GlobalStatsReceived_t);
		  STEAM_CALLBACK(Steam, _LeaderboardFindResult_t,                 LeaderboardFindResult_t);
		  STEAM_CALLBACK(Steam, _LeaderboardScoresDownloaded_t,           LeaderboardScoresDownloaded_t);
		  STEAM_CALLBACK(Steam, _LeaderboardScoreUploaded_t,              LeaderboardScoreUploaded_t);
		//STEAM_CALLBACK(Steam, _LeaderboardUGCSet_t,                     LeaderboardUGCSet_t);
		//STEAM_CALLBACK(Steam, _NumberOfCurrentPlayers_t,                NumberOfCurrentPlayers_t);
		//STEAM_CALLBACK(Steam, _PS3TrophiesInstalled_t,                  PS3TrophiesInstalled_t);
		//STEAM_CALLBACK(Steam, _UserAchievementIconFetched_t,            UserAchievementIconFetched_t);
		//STEAM_CALLBACK(Steam, _UserAchievementStored_t,                 UserAchievementStored_t);
		//STEAM_CALLBACK(Steam, _UserStatsReceived_t,                     UserStatsReceived_t);
		//STEAM_CALLBACK(Steam, _UserStatsStored_t,                       UserStatsStored_t);
		//STEAM_CALLBACK(Steam, _UserStatsUnloaded_t,                     UserStatsUnloaded_t);
		//
		//// Utils
		//STEAM_CALLBACK(Steam, _CheckFileSignature_t,                    CheckFileSignature_t);
		//STEAM_CALLBACK(Steam, _GamepadTextInputDismissed_t,             GamepadTextInputDismissed_t);
		//STEAM_CALLBACK(Steam, _IPCountry_t,                             IPCountry_t);
		  STEAM_CALLBACK(Steam, _LowBatteryPower_t,                       LowBatteryPower_t);
		//STEAM_CALLBACK(Steam, _SteamAPICallCompleted_t,                 SteamAPICallCompleted_t);
		//STEAM_CALLBACK(Steam, _SteamShutdown_t,                         SteamShutdown_t);
		//
		//// Video
		//STEAM_CALLBACK(Steam, _BroadcastUploadStart_t,                  BroadcastUploadStart_t);
		//STEAM_CALLBACK(Steam, _BroadcastUploadStop_t,                   BroadcastUploadStop_t);
		//STEAM_CALLBACK(Steam, _GetOPFSettingsResult_t,                  GetOPFSettingsResult_t);
		//STEAM_CALLBACK(Steam, _GetVideoURLResult_t,                     GetVideoURLResult_t);
	};

#endif // STEAM_POWER_H
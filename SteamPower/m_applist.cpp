#include "main.h"
#include "m_friends.h"
#include <string>
extern Steam * steam;

int SteamPower_applist_GetAppName(lua_State * L) {
	uint64 AppID = luax_checkint_64(L, 1);
	char * Buf = new char[200];

	int len = SteamAppList()->GetAppName(AppID, Buf, 200);
	string AppName = string(Buf);
	luax_pushString(L, AppName);
	steam->debug("Applist: app name by id [%s]: [%s]", uint64ToChar(AppID), AppName.c_str());
	delete Buf;
	return 1;
}

void SteamPower_applist_register(lua_State * L) {
	lua_pushstring(L, "applist");
	lua_newtable(L);
	luax_tfunction(L, "GetAppName", SteamPower_applist_GetAppName);
	lua_rawset(L, -3);
}

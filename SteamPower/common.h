#pragma once
#ifndef COMMON_H
#define COMMON_H
#define _CRT_SECURE_NO_WARNINGS

	extern "C" {
		#include "lua.h"
		#include "lauxlib.h"
	}

	#include "memory"
	#include <vector>
	#include <string>
	#include <iostream>
	#include <stdarg.h>
	#include "steam_api.h"
	using namespace std;



	// serealize int64 to char *
	const char * uint64ToChar(uint64 num);

	// serealize int64 to std::string
	string uint64ToString(uint64 num);

	// deserealize int64 to std::string
	uint64 stringToUint64(std::string str);

	// print current content of lua-stack to std out
	void luax_printstack(lua_State * L);

	// return uint64 on top of stack
	uint64 luax_checkint_64(lua_State * L, int idx);
	// push int64 to top of stack
	void luax_pushint_64(lua_State * L, uint64 value);

	// push int_64 transformed to string to table on top of stack with key/value as char *, int_64
	void luax_tint_64(lua_State * L, char * key, uint64 value);
	// push int_64 transformed to string to table on top of stack with key/value as int, int_64
	void luax_tint_64(lua_State * L, int    key, uint64 value);

	// push string to table on top of stack with key/value as char *, char *
	void luax_tstring(lua_State * L, char * key, const char * value);
	// push string to table on top of stack with key/value as int, char *
	void luax_tstring(lua_State * L, int    key, const char * value);

	// push float to table on top of stack with key/value as char *, int
	void luax_tnumber(lua_State * L, char * key, float value);
	// push float to table on top of stack with key/value as int, int
	void luax_tnumber(lua_State * L, int    key, float value);

	// push boolean to table on top of stack with key/value as char *, bool
	void luax_tboolean(lua_State * L, char * key, bool value);
	// push boolean to table on top of stack with key/value as int, bool
	void luax_tboolean(lua_State * L, int    key, bool value);

	// push function to table on top of stack with key/value as char *, cfunction
	void luax_tfunction(lua_State * L, char * key, lua_CFunction value);
	// push function to table on top of stack with key/value as int, cfunction
	void luax_tfunction(lua_State * L, int    key, lua_CFunction value);

	// return std::string on top of stack
	std::string luax_toString(lua_State * L,    int idx);
	// return std::string on top of stack
	std::string luax_checkString(lua_State * L, int idx);
	// push std::string to top of stack
	void luax_pushString(lua_State * L, const std::string & str);
	// push std::string to table on top of stack with key/value as char *, std::string
	void luax_tString(lua_State * L, char * key, const std::string & str);
	// push std::string to table on top of stack with key/value as int, std::string
	void luax_tString(lua_State * L, int    key, const std::string & str);


#endif // COMMON_H




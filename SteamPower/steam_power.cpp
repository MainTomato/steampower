#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <ctime>
#include "steam_power.h"
using namespace std;

// translates to string
char * EResult_string[] = {
	"None",
	"Success",
	"Generic failure",
	"Your Steam client doesn't have a connection to the back-end",
	"Password/ticket is invalid",
	"The user is logged in elsewhere",
	"Protocol version is incorrect",
	"A parameter is incorrect",
	"File was not found",
	"Called method is busy - action not taken",
	"Called object was in an invalid state",
	"The name was invalid",
	"The email was invalid",
	"The name is not unique",
	"Access is denied",
	"Operation timed out",
	"The user is VAC2 banned",
	"The Steam ID was invalid",
	"The requested service is currently unavailable",
	"The user is not logged on",
	"Request is pending, it may be in process or waiting on third party",
	"Encryption or Decryption failed",
	"Insufficient privilege",
	"Too much of a good thing",
	"Access has been revoked", // used for revoked guest passes.
	"License/Guest pass the user is trying to access is expired",
	"Guest pass has already been redeemed by account, cannot be used again",
	"The request is a duplicate and the action has already occurred in the past, ignored this time",
	"All the games in this guest pass redemption request are already owned by the user",
	"IP address not found",
	"Failed to write change to the data store",
	"Failed to acquire access lock for this operation",
	"The logon session has been replaced",
	"Failed to connect",
	"The authentication handshake has failed",
	"There has been a genic IO failure",
	"The remote server has disconnected",
	"Failed to find the shopping cart requested",
	"A user blocked the action",
	"The target is ignoring sender",
	"Nothing matching the request found",
	"The account is disabled",
	"This service is not accepting content changes right now",
	"Account doesn't have value, so this feature isn't available",
	"Allowed to take this action, but only because requester is admin",
	"A Version mismatch in content transmitted within the Steam protocol",
	"The current CM can't service the user making a request, user should try another",
	"You are already logged in elsewhere, this cached credential login has failed",
	"The user is logged in elsewhere",                     // Use k_EResultLoggedInElsewhere instead!
	"Long running operation has suspended/paused",         // eg. content download.
	"Operation has been canceled, typically by user",      // eg. content download.
	"Operation canceled because data is ill formed or unrecoverable",
	"Operation canceled - not enough disk space",
	"The remote or IPC call has failed",
	"Password could not be verified as it's unset server side",
	"External account (PSN, Facebook...) is not linked to a Steam account",
	"PSN ticket was invalid",
	"External account (PSN, Facebook...) is already linked to some other account, must explicitly request to replace/delete the link first",
	"The sync cannot resume due to a conflict between the local and remote files",
	"The requested new password is not allowed",
	"New value is the same as the old one. This is used for secret question and answer",
	"Account login denied due to 2nd factor authentication failure",
	"The requested new password is not legal",
	"Account login denied due to auth code invalid",
	"Account login denied due to 2nd factor auth failure - and no mail has been sent",
	"The users hardware does not support Intel's Identity Protection Technology (IPT)",
	"Intel's Identity Protection Technology (IPT) has failed to initialize",
	"Operation failed due to parental control restrictions for current user",
	"Facebook query returned an error",
	"Account login denied due to an expired auth code",
	"The login failed due to an IP resriction",
	"The current users account is currently locked for use. This is likely due to a hijacking and pending ownership verification",
	"The logon failed because the accounts email is not verified",
	"There is no URL matching the provided values",
	"Bad Response due to a Parse failure, missing field, etc",
	"The user cannot complete the action until they re-enter their password",
	"The value entered is outside the acceptable range",
	"Something happened that we didn't expect to ever happen",
	"The requested service has been configured to be unavailable",
	"The files submitted to the CEG server are not valid",
	"The device being used is not allowed to perform this action",
	"The action could not be complete because it is region restricted",
	"Temporary rate limit exceeded, try again later",      // different from k_EResultLimitExceeded which may be permanent
	"Need two-factor code to login",
	"The thing we're trying to access has been deleted",
	"Login attempt failed, try to throttle response to possible attacker",
	"Two factor authentication (Steam Guard) code is incorrect",
	"The activation code for two-factor authentication (Steam Guard) didn't match",
	"The current account has been associated with multiple partners",
	"The data has not been modified",
	"The account does not have a mobile device associated with it",
	"The time presented is out of range or tolerance",
	"SMS code failure - no match, none pending, etc",
	"Too many accounts access this resource",
	"Too many changes to this account",
	"Too many changes to this phone",
	"Cannot refund to payment method, must use wallet",
	"Cannot send an email",
	"Can't perform operation until payment has settled",
	"The user needs to provide a valid captcha",
	"A game server login token owned by this token's owner has been banned",
	"Game server owner is denied for some other reason such as account locked, community ban, vac ban, missing phone, etc",
	"The type of thing we were requested to act on is invalid",
	"The IP address has been banned from taking this action",
	"This Game Server Login Token (GSLT) has expired from disuse, it can be reset for use",
	"user doesn't have enough wallet funds to complete the action",
	"There are too many of this thing pending already",
};

char * EMouseCursor_string[] = {
	"user",
	"none",
	"arrow",
	"ibeam",
	"hourglass",
	"waitarrow",
	"crosshair",
	"up",
	"sizenw",
	"sizese",
	"sizene",
	"sizesw",
	"sizew",
	"sizee",
	"sizen",
	"sizes",
	"sizewe",
	"sizens",
	"sizeall",
	"no",
	"hand",
	"blank",
	"middle_pan",
	"north_pan",
	"north_east_pan",
	"east_pan",
	"south_east_pan",
	"south_pan",
	"south_west_pan",
	"west_pan",
	"north_west_pan",
	"alias",
	"cell",
	"colresize",
	"copycur",
	"verticaltext",
	"rowresize",
	"zoomin",
	"zoomout",
	"help",
	"custom",
	"last"
};

char * m_eChatRoomEnterResponse_String[] = {
	"None",
	"Success",
	"Chat doesn't exist (probably closed)",
	"General Denied - You don't have the permissions needed to join the chat",
	"Chat room has reached its maximum size",
	"Unexpected Error",
	"You are banned from this chat room and may not join",
	"Joining this chat is not allowed because you are a limited user (no value on account)",
	"Attempt to join a clan chat when the clan is locked or disabled",
	"Attempt to join a chat when the user has a community lock on their account",
	"Join failed - a user that is in the chat has blocked you from joining",
	"Join failed - you have blocked a user that is already in the chat",
};

char * EHTMLMouseButton_String[] = {
	"Left",
	"Right",
	"Middle",
};

char * EDenyReason_String[] = {
	"Invalid",
	"InvalidVersion",
	"Generic",
	"NotLoggedOn",
	"NoLicense",
	"Cheater",
	"LoggedInElseWhere",
	"UnknownText",
	"IncompatibleAnticheat",
	"MemoryCorruption",
	"IncompatibleSoftware",
	"SteamConnectionLost",
	"SteamConnectionError",
	"SteamResponseTimedOut",
	"SteamValidationStalled",
	"SteamOwnerLeftGuestUser",
};

char * EGameIDType_String[] = {
	"App",
	"GameMod",
	"Shortcut",
	"P2P",
};

int Steam::init(bool v){
	_dbg = v;

	if (SteamAPI_RestartAppIfNecessary(480)) {
		return 2;
		debug("Init: Restart app");
	}
	if (!SteamAPI_Init()) {
		debug("Init: failed: %d", SteamAPI_Init());
		return 3;
	}
	debug("Init: successed");

	initialized = true;
	return 1;
}

// prints in std output `date time [Steam lib]: "formatted message" \n`
void Steam::debug(const char* fmt, ...) {
	if (!_dbg) return;
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", timeinfo);
	std::string time_now(buffer);

	va_list ap;
	va_start(ap, fmt);
	const size_t MAX_LEN = 1023;
	char msg[MAX_LEN + 1] = { 0 };
	vsnprintf(msg, MAX_LEN, fmt, ap);
	std::cout << time_now << " [Steam lib]: " << msg << std::endl;
	va_end(ap);
}

bool Steam::getDownloadedLeaderboardEntry(SteamLeaderboardEntries_t eHandle, int entryCount) {
	if (SteamUserStats() == NULL) {
		return 0;
	}
	leaderboard_entries.clear();
	for (int i = 0; i < entryCount; i++) {
		LeaderboardEntry_t *entry = NULL;
		SteamUserStats()->GetDownloadedLeaderboardEntry(eHandle, i, entry, NULL, 0);
		SteamPower_e_leaderboard_rank e(entry);
		leaderboard_entries.push_back(e);
	}
	return 1;
}

event Steam::_event_poll() {
	if (event_list.empty())
		return std::make_shared<SteamPowerEvent>("Undefined");
	auto e = event_list.back();
	this->event_list.pop_back();
	return e;
}

void Steam::_event_push(event e) {
	debug("Event: Create new event [%s]", e->type.c_str());
	if (event_list.size() < MAX_EVENT_COUNT - 1)
		event_list.push_back(e);
}

void Steam::_event_clear() {
	debug("Event: Clear event: [%d] events deleted", event_list.size());
	event_list.clear();
}

int Steam::_event_flush() {
	if (!initialized) {
		debug("Event: event flush failed, steam not initialized");
		return 0;
	}
	SteamAPI_RunCallbacks();
	return 1;
}

int Steam::_event_count()
{
	debug("Event: event count: [%d] events now in list", event_list.size());
	return event_list.size();;
}


// AppList
// SteamAppInstalled_t
class SteamPower_e_SteamAppInstalled_t : public SteamPowerEvent {
public:
	uint32 AppID;
	SteamPower_e_SteamAppInstalled_t(SteamAppInstalled_t * t) : SteamPowerEvent("SteamAppInstalled") {
		AppID = t->m_nAppID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "AppID", AppID);
		return 0;
	};
};

void Steam::_SteamAppInstalled_t(SteamAppInstalled_t* t) {
	auto e = std::make_shared<SteamPower_e_SteamAppInstalled_t>(t);
	event_list.push_back(e);
}

// SteamAppUninstalled_t
class SteamPower_e_SteamAppUninstalled_t : public SteamPowerEvent {
public:
	uint32 AppID;
	SteamPower_e_SteamAppUninstalled_t(SteamAppUninstalled_t * t) : SteamPowerEvent("SteamAppUninstalled") {
		AppID = t->m_nAppID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "AppID", AppID);
		return 0;
	};
};

void Steam::_SteamAppUninstalled_t(SteamAppUninstalled_t* t) {
	auto e = std::make_shared<SteamPower_e_SteamAppUninstalled_t>(t);
	event_list.push_back(e);
}

// Apps
// AppProofOfPurchaseKeyResponse_t // internaly on steam

// DlcInstalled_t // called when dlc installed
class SteamPower_e_DlcInstalled_t : public SteamPowerEvent {
public:
	int AppID;
	SteamPower_e_DlcInstalled_t(DlcInstalled_t * t) : SteamPowerEvent("DlcInstalled") {
		AppID = t->m_nAppID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "AppID", AppID);
		return 0;
	};
};

void Steam::_DlcInstalled_t(DlcInstalled_t* t) {
	auto e = std::make_shared<SteamPower_e_DlcInstalled_t>(t);
	event_list.push_back(e);
}

// FileDetailsResult_t
class SteamPower_e_FileDetailsResult_t : public SteamPowerEvent {
public:
	uint64 FileSize, Flags;
	string FileSHA;
	SteamPower_e_FileDetailsResult_t(FileDetailsResult_t * t) : SteamPowerEvent("FileDetailsResult") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != 1) return;

		FileSize = t->m_ulFileSize;
		Flags    = t->m_unFlags;
		for (size_t i = 0; i < 20; ++i)
			FileSHA[i] = static_cast<char>(t->m_FileSHA[i]);

	};

	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "FileSize", FileSize);
		luax_tint_64(L, "Flags",    Flags);
		luax_tString(L, "FileSHA",  FileSHA);
		return 0;
	};
};

void Steam::_FileDetailsResult_t(FileDetailsResult_t* t) {
	auto e = std::make_shared<SteamPower_e_FileDetailsResult_t>(t);
	event_list.push_back(e);
}



// Friends
// AvatarImageLoaded_t // Signal that the avatar has been loaded
class SteamPower_e_AvatarImageLoaded_t : public SteamPowerEvent {
public:
	uint64 Steam_ID;
	string image;
	int width, height, lenth;
	SteamPower_e_AvatarImageLoaded_t(AvatarImageLoaded_t * t, string imgdata) : SteamPowerEvent("AvatarImageLoaded") {
		Steam_ID = t->m_steamID.GetStaticAccountKey();
		width    = t->m_iWide;
		height   = t->m_iTall;
		lenth    = 4 * width * height;
		image    = imgdata;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "Steam_ID",  Steam_ID);
		luax_tString(L, "Imagedata", image);
		luax_tnumber(L, "width",     width);
		luax_tnumber(L, "height",    height);
		luax_tnumber(L, "lenth",     lenth);
		return 0;
	};
};

void Steam::_AvatarImageLoaded_t(AvatarImageLoaded_t* t) {
	// Get image buffer
	int bsize = 4 * t->m_iWide * t->m_iTall;

	uint8 * iBuffer = new uint8[bsize];
	bool success = SteamUtils()->GetImageRGBA(t->m_iImage, iBuffer, bsize);

	if (!success)
		return debug("Event avatar loaded: Failed to load image buffer");

	std::string output(bsize, 0);
	for (size_t i = 0; i < bsize; ++i)
		output[i] = static_cast<char>(iBuffer[i]);

	auto e = std::make_shared<SteamPower_e_AvatarImageLoaded_t>(t, output);
	delete[] iBuffer;
	_event_push(e);
}


// ClanOfficerListResponse_t
class SteamPower_e_ClanOfficerListResponse_t : public SteamPowerEvent {
public:
	uint64 IDClan;
	int    Officers;
	bool   Success;
	SteamPower_e_ClanOfficerListResponse_t(ClanOfficerListResponse_t * t) : SteamPowerEvent("ClanOfficerListResponse") {
		IDClan   = t->m_steamIDClan.GetStaticAccountKey();
		Officers = t->m_cOfficers;
		Success  = t->m_bSuccess;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L,  "IDClan",   IDClan);
		luax_tnumber(L,  "Officers", Officers);
		luax_tboolean(L, "Success",  Success);

		return 0;
	};
};

void Steam::_ClanOfficerListResponse_t(ClanOfficerListResponse_t* t) {
	auto e = std::make_shared<SteamPower_e_ClanOfficerListResponse_t>(t);
	_event_push(e);
}

// DownloadClanActivityCountsResult_t
class SteamPower_e_DownloadClanActivityCountsResult_t : public SteamPowerEvent {
public:
	bool Success;
	SteamPower_e_DownloadClanActivityCountsResult_t(DownloadClanActivityCountsResult_t * t) : SteamPowerEvent("DownloadClanActivityCountsResult") {
		Success = t->m_bSuccess;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tboolean(L, "Success", Success);

		return 0;
	};
};

void Steam::_DownloadClanActivityCountsResult_t(DownloadClanActivityCountsResult_t* t) {
	auto e = std::make_shared<SteamPower_e_DownloadClanActivityCountsResult_t>(t);
	_event_push(e);
}

// FriendRichPresenceUpdate_t
class SteamPower_e_FriendRichPresenceUpdate_t : public SteamPowerEvent {
public:
	uint64 IDFriend, AppID;
	SteamPower_e_FriendRichPresenceUpdate_t(FriendRichPresenceUpdate_t * t) : SteamPowerEvent("FriendRichPresenceUpdate") {
		IDFriend = t->m_steamIDFriend.GetStaticAccountKey();
		AppID    = t->m_nAppID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDFriend", IDFriend);
		luax_tint_64(L, "AppID",    AppID);
		return 0;
	};
};

void Steam::_FriendRichPresenceUpdate_t(FriendRichPresenceUpdate_t* t) {
	auto e = std::make_shared<SteamPower_e_FriendRichPresenceUpdate_t>(t);
	_event_push(e);
}

// FriendsEnumerateFollowingList_t
class SteamPower_e_FriendsEnumerateFollowingList_t : public SteamPowerEvent {
public:
	int ResultsReturned, TotalResultCount;
	std::vector<uint64> SteamID;
	SteamPower_e_FriendsEnumerateFollowingList_t(FriendsEnumerateFollowingList_t * t) : SteamPowerEvent("FriendsEnumerateFollowingList") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;

		TotalResultCount = t->m_nTotalResultCount;
		ResultsReturned = t->m_nResultsReturned;
		for (int i = 0; i < ResultsReturned; ++i)
			SteamID.push_back(t->m_rgSteamID[i].GetStaticAccountKey());

	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1; // errors, automaticly push errors to lua
		lua_newtable(L);                                       // result = {}
		luax_tnumber(L, "TotalResultCount", TotalResultCount); // result.TotalResultCount = TotalResultCount
		luax_tnumber(L, "ResultsReturned",  ResultsReturned);  // result.ResultsReturned  = ResultsReturned
		lua_pushstring(L, "SteamID"); lua_newtable(L);         // result.SteamID = {}
			for (int i = 0; i < SteamID.size(); i++)           // for i = 0, #self.SteamID do
				luax_tint_64(L, i + 1, SteamID[i]);            // result.SteamID[i] = self.SteamID[i]
			lua_rawset(L, -3);                                 // part of "result.SteamID = {}"
		return 0;                                              // return result
	};
};

void Steam::_FriendsEnumerateFollowingList_t(FriendsEnumerateFollowingList_t* t) {
	auto e = std::make_shared<SteamPower_e_FriendsEnumerateFollowingList_t>(t);
	_event_push(e);
}

// FriendsGetFollowerCount_t
class SteamPower_e_FriendsGetFollowerCount_t : public SteamPowerEvent {
public:
	uint64 SteamID;
	int    Count;
	SteamPower_e_FriendsGetFollowerCount_t(FriendsGetFollowerCount_t * t) : SteamPowerEvent("FriendsGetFollowerCount") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;

		SteamID = t->m_steamID.GetStaticAccountKey();
		Count   = t->m_nCount;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "SteamID", SteamID);
		luax_tnumber(L, "Count",   Count);
		return 0;
	};
};

void Steam::_FriendsGetFollowerCount_t(FriendsGetFollowerCount_t* t) {
	auto e = std::make_shared<SteamPower_e_FriendsGetFollowerCount_t>(t);
	_event_push(e);
}

// FriendsIsFollowing_t
class SteamPower_e_FriendsIsFollowing_t : public SteamPowerEvent {
public:
	uint64 SteamID;
	bool   IsFollowing;
	SteamPower_e_FriendsIsFollowing_t(FriendsIsFollowing_t * t) : SteamPowerEvent("FriendsIsFollowing") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;

		SteamID     = t->m_steamID.GetStaticAccountKey();
		IsFollowing = t->m_bIsFollowing;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64 (L, "SteamID", SteamID);
		luax_tboolean(L, "IsFollowing", IsFollowing);
		return 0;
	};
};

void Steam::_FriendsIsFollowing_t(FriendsIsFollowing_t* t) {
	auto e = std::make_shared<SteamPower_e_FriendsIsFollowing_t>(t);
	_event_push(e);
}

// GameConnectedChatJoin_t
class SteamPower_e_GameConnectedChatJoin_t : public SteamPowerEvent {
public:
	uint64 IDClanChat, IDUser;
	SteamPower_e_GameConnectedChatJoin_t(GameConnectedChatJoin_t * t) : SteamPowerEvent("GameConnectedChatJoin") {
		IDClanChat = t->m_steamIDClanChat.GetStaticAccountKey();
		IDUser     = t->m_steamIDUser.GetStaticAccountKey();
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDClanChat", IDClanChat);
		luax_tint_64(L, "IDUser",     IDUser);
		return 0;
	};
};

void Steam::_GameConnectedChatJoin_t(GameConnectedChatJoin_t* t) {
	auto e = std::make_shared<SteamPower_e_GameConnectedChatJoin_t>(t);
	_event_push(e);
}

// GameConnectedChatLeave_t
class SteamPower_e_GameConnectedChatLeave_t : public SteamPowerEvent {
public:
	uint64 IDClanChat, IDUser;
	bool   Kicked, Dropped;
	SteamPower_e_GameConnectedChatLeave_t(GameConnectedChatLeave_t * t) : SteamPowerEvent("GameConnectedChatLeave") {
		IDClanChat = t->m_steamIDClanChat.GetStaticAccountKey();
		IDUser     = t->m_steamIDUser.GetStaticAccountKey();
		Kicked     = t->m_bKicked;
		Dropped    = t->m_bDropped;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64 (L, "IDClanChat", IDClanChat);
		luax_tint_64 (L, "IDUser",     IDUser);
		luax_tboolean(L, "Kicked",     Kicked);
		luax_tboolean(L, "Dropped",    Dropped);
		return 0;
	};
};

void Steam::_GameConnectedChatLeave_t(GameConnectedChatLeave_t* t) {
	auto e = std::make_shared<SteamPower_e_GameConnectedChatLeave_t>(t);
	_event_push(e);
}

// GameConnectedClanChatMsg_t
class SteamPower_e_GameConnectedClanChatMsg_t : public SteamPowerEvent {
public:
	uint64 IDClanChat, IDUser;
	int    MessageID;
	SteamPower_e_GameConnectedClanChatMsg_t(GameConnectedClanChatMsg_t * t) : SteamPowerEvent("GameConnectedClanChatMsg") {
		IDClanChat = t->m_steamIDClanChat.GetStaticAccountKey();
		IDUser     = t->m_steamIDUser.GetStaticAccountKey();
		MessageID  = t->m_iMessageID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDClanChat", IDClanChat);
		luax_tint_64(L, "IDUser",     IDUser);
		luax_tnumber(L, "MessageID",  MessageID);
		return 0;
	};
};

void Steam::_GameConnectedClanChatMsg_t(GameConnectedClanChatMsg_t* t) {
	auto e = std::make_shared<SteamPower_e_GameConnectedClanChatMsg_t>(t);
	_event_push(e);
}


// GameConnectedFriendChatMsg_t
class SteamPower_e_GameConnectedFriendChatMsg_t : public SteamPowerEvent {
public:
	uint64 IDUser;
	int    MessageID;
	SteamPower_e_GameConnectedFriendChatMsg_t(GameConnectedFriendChatMsg_t * t) : SteamPowerEvent("GameConnectedFriendChatMsg") {
		IDUser = t->m_steamIDUser.GetStaticAccountKey();
		MessageID = t->m_iMessageID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDUser",    IDUser);
		luax_tnumber(L, "MessageID", MessageID);
		return 0;
	};
};

void Steam::_GameConnectedFriendChatMsg_t(GameConnectedFriendChatMsg_t* t) {
	auto e = std::make_shared<SteamPower_e_GameConnectedFriendChatMsg_t>(t);
	_event_push(e);
}

// GameLobbyJoinRequested_t
class SteamPower_e_GameLobbyJoinRequested_t : public SteamPowerEvent {
public:
	uint64 IDLobby, IDFriend;
	SteamPower_e_GameLobbyJoinRequested_t(GameLobbyJoinRequested_t * t) : SteamPowerEvent("GameLobbyJoinRequested") {
		IDLobby  = t->m_steamIDLobby.GetStaticAccountKey();
		IDFriend = t->m_steamIDFriend.GetStaticAccountKey();
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDLobby",  IDLobby);
		luax_tint_64(L, "IDFriend", IDFriend);
		return 0;
	};
};

void Steam::_GameLobbyJoinRequested_t(GameLobbyJoinRequested_t* t) {
	auto e = std::make_shared<SteamPower_e_GameLobbyJoinRequested_t>(t);
	_event_push(e);
}

// GameOverlayActivated_t // Signal when overlay is triggered
	class SteamPower_e_GameOverlayActivated_t : public SteamPowerEvent {
	public:
		int Active;
		SteamPower_e_GameOverlayActivated_t(GameOverlayActivated_t * t) : SteamPowerEvent("GameOverlayActivated") {
			Active = t->m_bActive;
		};
		virtual bool tolua(lua_State * L) {
			if (!SteamPowerEvent::tolua(L)) return 1;
			lua_newtable(L);
			luax_tnumber(L, "Active", Active);
			return 0;
		};
	};

void Steam::_GameOverlayActivated_t(GameOverlayActivated_t* t) {
	auto e = std::make_shared<SteamPower_e_GameOverlayActivated_t>(t);
	_event_push(e);
}


// GameRichPresenceJoinRequested_t // Signal a game/lobby join has been requested
	class SteamPower_e_GameRichPresenceJoinRequested_t : public SteamPowerEvent {
	public:
		uint64 IDFriend;
		string Connect;
		SteamPower_e_GameRichPresenceJoinRequested_t(GameRichPresenceJoinRequested_t * t) : SteamPowerEvent("GameRichPresenceJoinRequested") {
			IDFriend = t->m_steamIDFriend.GetStaticAccountKey();
			Connect  = t->m_rgchConnect;
		};
		virtual bool tolua(lua_State * L) {
			if (!SteamPowerEvent::tolua(L)) return 1;
			lua_newtable(L);
			luax_tint_64(L, "IDFriend", IDFriend);
			luax_tString(L, "Connect",  Connect);
			return 0;
		};
	};

void Steam::_GameRichPresenceJoinRequested_t(GameRichPresenceJoinRequested_t *t) {
	auto e = std::make_shared<SteamPower_e_GameRichPresenceJoinRequested_t>(t);
	_event_push(e);
}

// GameServerChangeRequested_t
class SteamPower_e_GameServerChangeRequested_t : public SteamPowerEvent {
public:
	string Server, Password;
	SteamPower_e_GameServerChangeRequested_t(GameServerChangeRequested_t * t) : SteamPowerEvent("GameServerChangeRequested") {
		Server   = t->m_rgchServer; // check how char[?] transforms into std::string
		Password = t->m_rgchPassword;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tString(L, "Server",   Server);
		luax_tString(L, "Password", Password);
		return 0;
	};
};

void Steam::_GameServerChangeRequested_t(GameServerChangeRequested_t* t) {
	auto e = std::make_shared<SteamPower_e_GameServerChangeRequested_t>(t);
	_event_push(e);
}

// JoinClanChatRoomCompletionResult_t
class SteamPower_e_JoinClanChatRoomCompletionResult_t : public SteamPowerEvent {
public:
	uint64 IDClanChat;
	SteamPower_e_JoinClanChatRoomCompletionResult_t(JoinClanChatRoomCompletionResult_t * t) : SteamPowerEvent("JoinClanChatRoomCompletionResult") {
		Result_code = t->m_eChatRoomEnterResponse;
		error = m_eChatRoomEnterResponse_String[t->m_eChatRoomEnterResponse];
		if (Result_code != 1) return;
		IDClanChat = t->m_steamIDClanChat.GetStaticAccountKey();
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDClanChat", IDClanChat);
		return 0;
	};
};

void Steam::_JoinClanChatRoomCompletionResult_t(JoinClanChatRoomCompletionResult_t* t) {
	auto e = std::make_shared<SteamPower_e_JoinClanChatRoomCompletionResult_t>(t);
	_event_push(e);
}

// PersonaStateChange_t
class SteamPower_e_PersonaStateChange_t : public SteamPowerEvent {
public:
	uint64 SteamID;
	int    ChangeFlags;
	string ChangeString;
	SteamPower_e_PersonaStateChange_t(PersonaStateChange_t * t) : SteamPowerEvent("PersonaStateChange") {
		SteamID     = t->m_ulSteamID;
		ChangeFlags = t->m_nChangeFlags;
		if      (ChangeFlags == 0x0001) ChangeString = "Name";
		else if (ChangeFlags == 0x0002) ChangeString = "Status";
		else if (ChangeFlags == 0x0004) ChangeString = "ComeOnline";
		else if (ChangeFlags == 0x0008) ChangeString = "ComeOffline";
		else if (ChangeFlags == 0x0010) ChangeString = "GamePlayed";
		else if (ChangeFlags == 0x0020) ChangeString = "GameServer";
		else if (ChangeFlags == 0x0040) ChangeString = "Avatar";
		else if (ChangeFlags == 0x0080) ChangeString = "JoinedSource";
		else if (ChangeFlags == 0x0100) ChangeString = "LeftSource";
		else if (ChangeFlags == 0x0200) ChangeString = "RelationshipChanged";
		else if (ChangeFlags == 0x0400) ChangeString = "NameFirstSet";
		else if (ChangeFlags == 0x0800) ChangeString = "FacebookInfo";
		else if (ChangeFlags == 0x1000) ChangeString = "Nickname";
		else if (ChangeFlags == 0x2000) ChangeString = "SteamLevel";
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "SteamID",      SteamID);
		luax_tnumber(L, "ChangeFlags",  ChangeFlags);
		luax_tString(L, "ChangeString", ChangeString);
		return 0;
	};
};

void Steam::_PersonaStateChange_t(PersonaStateChange_t* t) {
	auto e = std::make_shared<SteamPower_e_PersonaStateChange_t>(t);
	_event_push(e);
}

// SetPersonaNameResponse_t
class SteamPower_e_SetPersonaNameResponse_t : public SteamPowerEvent {
public:
	bool Success, LocalSuccess;
	SteamPower_e_SetPersonaNameResponse_t(SetPersonaNameResponse_t * t) : SteamPowerEvent("SetPersonaNameResponse") {
		Result_code = t->m_result;
		error = EResult_string[t->m_result];
		if (t->m_result != k_EResultOK) return;
		Success      = t->m_bSuccess;
		LocalSuccess = t->m_bLocalSuccess;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tboolean(L, "Success",      Success);
		luax_tboolean(L, "LocalSuccess", LocalSuccess);
		return 0;
	};
};

void Steam::_SetPersonaNameResponse_t(SetPersonaNameResponse_t* t) {
	auto e = std::make_shared<SteamPower_e_SetPersonaNameResponse_t>(t);
	_event_push(e);
}

// HTMLSurface
// HTML_BrowserReady_t
class SteamPower_e_HTML_BrowserReady_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	SteamPower_e_HTML_BrowserReady_t(HTML_BrowserReady_t * t) : SteamPowerEvent("HTML_BrowserReady") {
		BrowserHandle = t->unBrowserHandle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		return 0;
	};
};

void Steam::_HTML_BrowserReady_t(HTML_BrowserReady_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_BrowserReady_t>(t);
	_event_push(e);
}


// HTML_CanGoBackAndForward_t
class SteamPower_e_HTML_CanGoBackAndForward_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	bool   CanGoBack, CanGoForward;
	SteamPower_e_HTML_CanGoBackAndForward_t(HTML_CanGoBackAndForward_t * t) : SteamPowerEvent("HTML_CanGoBackAndForward") {
		BrowserHandle = t->unBrowserHandle;
		CanGoBack     = t->bCanGoBack;
		CanGoForward  = t->bCanGoForward;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tboolean(L, "CanGoBack",     CanGoBack);
		luax_tboolean(L, "CanGoForward",  CanGoForward);
		return 0;
	};
};

void Steam::_HTML_CanGoBackAndForward_t(HTML_CanGoBackAndForward_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_CanGoBackAndForward_t>(t);
	_event_push(e);
}

// HTML_ChangedTitle_t
class SteamPower_e_HTML_ChangedTitle_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Title;
	SteamPower_e_HTML_ChangedTitle_t(HTML_ChangedTitle_t * t) : SteamPowerEvent("HTML_ChangedTitle") {
		BrowserHandle = t->unBrowserHandle;
		Title =         t->pchTitle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Title",         Title);
		return 0;
	};
};

void Steam::_HTML_ChangedTitle_t(HTML_ChangedTitle_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_ChangedTitle_t>(t);
	_event_push(e);
}

// HTML_CloseBrowser_t
class SteamPower_e_HTML_CloseBrowser_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	SteamPower_e_HTML_CloseBrowser_t(HTML_CloseBrowser_t * t) : SteamPowerEvent("HTML_CloseBrowser") {
		BrowserHandle = t->unBrowserHandle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		return 0;
	};
};

void Steam::_HTML_CloseBrowser_t(HTML_CloseBrowser_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_CloseBrowser_t>(t);
	_event_push(e);
}

// HTML_FileOpenDialog_t
class SteamPower_e_HTML_FileOpenDialog_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Title, InitialFile;
	SteamPower_e_HTML_FileOpenDialog_t(HTML_FileOpenDialog_t * t) : SteamPowerEvent("HTML_FileOpenDialog") {
		BrowserHandle = t->unBrowserHandle;
		Title = t->pchTitle;
		InitialFile = t->pchInitialFile;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Title",         Title);
		luax_tString(L, "InitialFile",   InitialFile);
		return 0;
	};
};

void Steam::_HTML_FileOpenDialog_t(HTML_FileOpenDialog_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_FileOpenDialog_t>(t);
	_event_push(e);
}

// HTML_FinishedRequest_t
class SteamPower_e_HTML_FinishedRequest_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string URL, PageTitle;
	SteamPower_e_HTML_FinishedRequest_t(HTML_FinishedRequest_t * t) : SteamPowerEvent("HTML_FinishedRequest") {
		BrowserHandle = t->unBrowserHandle;
		URL           = t->pchURL;
		PageTitle     = t->pchPageTitle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "URL",           URL);
		luax_tString(L, "PageTitle",     PageTitle);
		return 0;
	};
};

void Steam::_HTML_FinishedRequest_t(HTML_FinishedRequest_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_FinishedRequest_t>(t);
	_event_push(e);
}


// HTML_HideToolTip_t
class SteamPower_e_HTML_HideToolTip_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	SteamPower_e_HTML_HideToolTip_t(HTML_HideToolTip_t * t) : SteamPowerEvent("HTML_HideToolTip") {
		BrowserHandle = t->unBrowserHandle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		return 0;
	};
};

void Steam::_HTML_HideToolTip_t(HTML_HideToolTip_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_HideToolTip_t>(t);
	_event_push(e);
}


// HTML_HorizontalScroll_t
class SteamPower_e_HTML_HorizontalScroll_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, ScrollMax, ScrollCurrent, PageSize;
	float  PageScale;
	bool   Visible;
	SteamPower_e_HTML_HorizontalScroll_t(HTML_HorizontalScroll_t * t) : SteamPowerEvent("HTML_HorizontalScroll") {
		BrowserHandle = t->unBrowserHandle;
		ScrollMax     = t->unScrollMax;
		ScrollCurrent = t->unScrollCurrent;
		PageSize      = t->unPageSize;
		PageScale     = t->flPageScale;
		Visible       = t->bVisible;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tnumber (L, "ScrollMax",     ScrollMax);
		luax_tnumber (L, "ScrollCurrent", ScrollCurrent);
		luax_tnumber (L, "PageSize",      PageSize);
		luax_tnumber (L, "PageScale",     PageScale);
		luax_tboolean(L, "Visible",       Visible);
		return 0;
	};
};

void Steam::_HTML_HorizontalScroll_t(HTML_HorizontalScroll_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_HorizontalScroll_t>(t);
	_event_push(e);
}

// HTML_JSAlert_t
class SteamPower_e_HTML_JSAlert_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Message;
	SteamPower_e_HTML_JSAlert_t(HTML_JSAlert_t * t) : SteamPowerEvent("HTML_JSAlert") {
		BrowserHandle = t->unBrowserHandle;
		Message       = t->pchMessage;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Message",       Message);
		return 0;
	};
};

void Steam::_HTML_JSAlert_t(HTML_JSAlert_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_JSAlert_t>(t);
	_event_push(e);
}

// HTML_JSConfirm_t
class SteamPower_e_HTML_JSConfirm_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Message;
	SteamPower_e_HTML_JSConfirm_t(HTML_JSConfirm_t * t) : SteamPowerEvent("HTML_JSConfirm") {
		BrowserHandle = t->unBrowserHandle;
		Message = t->pchMessage;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Message",       Message);
		return 0;
	};
};

void Steam::_HTML_JSConfirm_t(HTML_JSConfirm_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_JSConfirm_t>(t);
	_event_push(e);
}

// HTML_LinkAtPosition_t
class SteamPower_e_HTML_LinkAtPosition_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, x, y;
	string URL;
	bool   Input, LiveLink;
	SteamPower_e_HTML_LinkAtPosition_t(HTML_LinkAtPosition_t * t) : SteamPowerEvent("HTML_LinkAtPosition") {
		BrowserHandle = t->unBrowserHandle;
		x        = t->x;
		y        = t->y;
		URL      = t->pchURL;
		Input    = t->bInput;
		LiveLink = t->bLiveLink;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tnumber (L, "x",             x);
		luax_tnumber (L, "y",             y);
		luax_tString (L, "URL",           URL);
		luax_tboolean(L, "Input",         Input);
		luax_tboolean(L, "LiveLink",      LiveLink);
		return 0;
	};
};

void Steam::_HTML_LinkAtPosition_t(HTML_LinkAtPosition_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_LinkAtPosition_t>(t);
	_event_push(e);
}

// HTML_NeedsPaint_t
class SteamPower_e_HTML_NeedsPaint_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, Wide, Tall, UpdateX, UpdateY, UpdateWide, UpdateTall, ScrollX, ScrollY, PageSerial;
	string BGRA;
	float  PageScale;
	SteamPower_e_HTML_NeedsPaint_t(HTML_NeedsPaint_t * t) : SteamPowerEvent("HTML_NeedsPaint") {
		BrowserHandle = t->unBrowserHandle;
		Wide          = t->unWide;
		Tall          = t->unTall;
		UpdateX       = t->unUpdateX;
		UpdateY       = t->unUpdateY;
		UpdateWide    = t->unUpdateWide;
		UpdateTall    = t->unUpdateTall;
		ScrollX       = t->unScrollX;
		ScrollY       = t->unScrollY;
		PageSerial    = t->unPageSerial;
		BGRA          = t->pBGRA;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tnumber(L, "Wide",          Wide);
		luax_tnumber(L, "Tall",          Tall);
		luax_tnumber(L, "UpdateX",       UpdateX);
		luax_tnumber(L, "UpdateY",       UpdateY);
		luax_tnumber(L, "UpdateWide",    UpdateWide);
		luax_tnumber(L, "UpdateTall",    UpdateTall);
		luax_tnumber(L, "ScrollX",       ScrollX);
		luax_tnumber(L, "ScrollY",       ScrollY);
		luax_tnumber(L, "PageSerial",    PageSerial);
		luax_tString(L, "BGRA",          BGRA);
		return 0;
	};
};

void Steam::_HTML_NeedsPaint_t(HTML_NeedsPaint_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_NeedsPaint_t>(t);
	_event_push(e);
}

// HTML_NewWindow_t
class SteamPower_e_HTML_NewWindow_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, X, Y, Wide, Tall, NewWindow_BrowserHandle;
	string URL;
	SteamPower_e_HTML_NewWindow_t(HTML_NewWindow_t * t) : SteamPowerEvent("HTML_NewWindow") {
		BrowserHandle           = t->unBrowserHandle;
		URL                     = t->pchURL;
		X                       = t->unX;
		Y                       = t->unY;
		Wide                    = t->unWide;
		Tall                    = t->unTall;
		NewWindow_BrowserHandle = t->unNewWindow_BrowserHandle;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle",           BrowserHandle);
		luax_tString(L, "URL",                     URL);
		luax_tnumber(L, "X",                       X);
		luax_tnumber(L, "Y",                       Y);
		luax_tnumber(L, "Wide",                    Wide);
		luax_tnumber(L, "Tall",                    Tall);
		luax_tnumber(L, "NewWindow_BrowserHandle", NewWindow_BrowserHandle);
		return 0;
	};
};

void Steam::_HTML_NewWindow_t(HTML_NewWindow_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_NewWindow_t>(t);
	_event_push(e);
}

// HTML_OpenLinkInNewTab_t
class SteamPower_e_HTML_OpenLinkInNewTab_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string URL;
	SteamPower_e_HTML_OpenLinkInNewTab_t(HTML_OpenLinkInNewTab_t * t) : SteamPowerEvent("HTML_OpenLinkInNewTab") {
		BrowserHandle = t->unBrowserHandle;
		URL           = t->pchURL;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "URL",           URL);
		return 0;
	};
};

void Steam::_HTML_OpenLinkInNewTab_t(HTML_OpenLinkInNewTab_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_OpenLinkInNewTab_t>(t);
	_event_push(e);
}

// HTML_SearchResults_t
class SteamPower_e_HTML_SearchResults_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, Results, CurrentMatch;
	SteamPower_e_HTML_SearchResults_t(HTML_SearchResults_t * t) : SteamPowerEvent("HTML_SearchResults") {
		BrowserHandle = t->unBrowserHandle;
		Results       = t->unResults;
		CurrentMatch  = t->unCurrentMatch;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tnumber(L, "Results",       Results);
		luax_tnumber(L, "CurrentMatch",  CurrentMatch);
		return 0;
	};
};

void Steam::_HTML_SearchResults_t(HTML_SearchResults_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_SearchResults_t>(t);
	_event_push(e);
}

// HTML_SetCursor_t
class SteamPower_e_HTML_SetCursor_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, MouseCursor;
	SteamPower_e_HTML_SetCursor_t(HTML_SetCursor_t * t) : SteamPowerEvent("HTML_SetCursor") {
		BrowserHandle = t->unBrowserHandle;
		MouseCursor   = t->eMouseCursor;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle",     BrowserHandle);
		luax_tnumber(L, "MouseCursor",       MouseCursor);
		luax_tString(L, "MouseCursorString", EMouseCursor_string[MouseCursor]);
		return 0;
	};
};

void Steam::_HTML_SetCursor_t(HTML_SetCursor_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_SetCursor_t>(t);
	_event_push(e);
}


// HTML_ShowToolTip_t
class SteamPower_e_HTML_ShowToolTip_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Msg;
	SteamPower_e_HTML_ShowToolTip_t(HTML_ShowToolTip_t * t) : SteamPowerEvent("HTML_ShowToolTip") {
		BrowserHandle = t->unBrowserHandle;
		Msg           = t->pchMsg;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Msg",           Msg);
		return 0;
	};
};

void Steam::_HTML_ShowToolTip_t(HTML_ShowToolTip_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_ShowToolTip_t>(t);
	_event_push(e);
}

// HTML_StartRequest_t
class SteamPower_e_HTML_StartRequest_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string URL, Target, PostData;
	bool   IsRedirect;
	SteamPower_e_HTML_StartRequest_t(HTML_StartRequest_t * t) : SteamPowerEvent("HTML_StartRequest") {
		BrowserHandle = t->unBrowserHandle;
		URL           = t->pchURL;
		Target        = t->pchTarget;
		PostData      = t->pchPostData;
		IsRedirect    = t->bIsRedirect;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tString (L, "URL",           URL);
		luax_tString (L, "Target",        Target);
		luax_tString (L, "PostData",      PostData);
		luax_tboolean(L, "IsRedirect",    IsRedirect);
		return 0;
	};
};

void Steam::_HTML_StartRequest_t(HTML_StartRequest_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_StartRequest_t>(t);
	SteamHTMLSurface()->AllowStartRequest(t->unBrowserHandle, true); // START REQUEST AUTOMATICLY ALLOWED!!!!
	_event_push(e);
}

// HTML_StatusText_t
class SteamPower_e_HTML_StatusText_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Msg;
	SteamPower_e_HTML_StatusText_t(HTML_StatusText_t * t) : SteamPowerEvent("HTML_StatusText") {
		BrowserHandle = t->unBrowserHandle;
		Msg           = t->pchMsg;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Msg",           Msg);
		return 0;
	};
};

void Steam::_HTML_StatusText_t(HTML_StatusText_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_StatusText_t>(t);
	_event_push(e);
}


// HTML_UpdateToolTip_t
class SteamPower_e_HTML_UpdateToolTip_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string Msg;
	SteamPower_e_HTML_UpdateToolTip_t(HTML_UpdateToolTip_t * t) : SteamPowerEvent("HTML_UpdateToolTip") {
		BrowserHandle = t->unBrowserHandle;
		Msg = t->pchMsg;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "BrowserHandle", BrowserHandle);
		luax_tString(L, "Msg",           Msg);
		return 0;
	};
};

void Steam::_HTML_UpdateToolTip_t(HTML_UpdateToolTip_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_UpdateToolTip_t>(t);
	_event_push(e);
}


// HTML_URLChanged_t
class SteamPower_e_HTML_URLChanged_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle;
	string URL, PostData, PageTitle;
	bool   IsRedirect, NewNavigation;
	SteamPower_e_HTML_URLChanged_t(HTML_URLChanged_t * t) : SteamPowerEvent("HTML_URLChanged") {
		BrowserHandle = t->unBrowserHandle;
		URL           = t->pchURL;
		PostData      = t->pchPostData;
		PageTitle     = t->pchPageTitle;
		IsRedirect    = t->bIsRedirect;
		NewNavigation = t->bNewNavigation;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tString (L, "URL",           URL);
		luax_tString (L, "PostData",      PostData);
		luax_tString (L, "PageTitle",     PageTitle);
		luax_tboolean(L, "IsRedirect",    IsRedirect);
		luax_tboolean(L, "NewNavigation", NewNavigation);
		return 0;
	};
};

void Steam::_HTML_URLChanged_t(HTML_URLChanged_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_URLChanged_t>(t);
	_event_push(e);
}

// HTML_VerticalScroll_t
class SteamPower_e_HTML_VerticalScroll_t : public SteamPowerEvent {
public:
	uint32 BrowserHandle, ScrollMax, ScrollCurrent, PageSize;
	float  PageScale;
	bool   Visible;
	SteamPower_e_HTML_VerticalScroll_t(HTML_VerticalScroll_t * t) : SteamPowerEvent("HTML_VerticalScroll") {
		BrowserHandle = t->unBrowserHandle;
		ScrollMax     = t->unScrollMax;
		ScrollCurrent = t->unScrollCurrent;
		PageSize      = t->unPageSize;
		PageScale     = t->flPageScale;
		Visible       = t->bVisible;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber (L, "BrowserHandle", BrowserHandle);
		luax_tnumber (L, "ScrollMax",     ScrollMax);
		luax_tnumber (L, "ScrollCurrent", ScrollCurrent);
		luax_tnumber (L, "PageSize",      PageSize);
		luax_tnumber (L, "PageScale",     PageScale);
		luax_tboolean(L, "Visible",       Visible);
		return 0;
	};
};

void Steam::_HTML_VerticalScroll_t(HTML_VerticalScroll_t* t) {
	auto e = std::make_shared<SteamPower_e_HTML_VerticalScroll_t>(t);
	_event_push(e);
}


// HTTP
// HTTPRequestCompleted_t
// HTTPRequestDataReceived_t
// HTTPRequestHeadersReceived_t

// Inventory
// SteamInventoryDefinitionUpdate_t
// SteamInventoryEligiblePromoItemDefIDs_t
// SteamInventoryFullUpdate_t
// SteamInventoryResultReady_t

// Matchmaking
// FavoritesListAccountsUpdated_t
// FavoritesListChanged_t
// LobbyChatMsg_t
// LobbyChatUpdate_t
// LobbyCreated_t // Signal that lobby has been created
class SteamPower_e_LobbyCreated_t : public SteamPowerEvent {
public:
	uint64 IDLobby;
	SteamPower_e_LobbyCreated_t(LobbyCreated_t * t) : SteamPowerEvent("LobbyCreated") {
		Result_code = t->m_eResult;
		error       = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;
		IDLobby     = t->m_ulSteamIDLobby;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDLobby", IDLobby);
		return 0;
	};
};

void Steam::_LobbyCreated_t(LobbyCreated_t* t) {
	auto e = std::make_shared<SteamPower_e_LobbyCreated_t>(t);
	_event_push(e);
}

// LobbyDataUpdate_t
// LobbyEnter_t

// LobbyInvite_t // Signal that a lobby invite was sent
class SteamPower_e_LobbyInvite_t : public SteamPowerEvent {
public:
	uint64 IDUser, IDLobby, GameID;
	SteamPower_e_LobbyInvite_t(LobbyInvite_t * t) : SteamPowerEvent("LobbyInvite") {
		IDUser  = t->m_ulSteamIDUser;
		IDLobby = t->m_ulSteamIDLobby;
		GameID  = t->m_ulGameID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "IDLobby", IDLobby);
		luax_tint_64(L, "IDUser",  IDUser);
		luax_tint_64(L, "GameID",  GameID);
		return 0;
	};
};

void Steam::_LobbyInvite_t(LobbyInvite_t* t) {
	auto e = std::make_shared<SteamPower_e_LobbyInvite_t>(t);
	_event_push(e);
}

// LobbyGameCreated_t
// LobbyKicked_t
// LobbyMatchList_t
// PSNGameBootInviteResult_t

// Music
// PlaybackStatusHasChanged_t
// VolumeHasChanged_t

// MusicRemote
// MusicPlayerRemoteToFront_t
// MusicPlayerRemoteWillActivate_t
// MusicPlayerRemoteWillDeactivate_t
// MusicPlayerSelectsPlaylistEntry_t
// MusicPlayerSelectsQueueEntry_t
// MusicPlayerWantsLooped_t
// MusicPlayerWantsPause_t
// MusicPlayerWantsPlayingRepeatStatus_t
// MusicPlayerWantsPlayNext_t
// MusicPlayerWantsPlayPrevious_t
// MusicPlayerWantsPlay_t
// MusicPlayerWantsShuffled_t
// MusicPlayerWantsVolume_t
// MusicPlayerWillQuit_t

// Networking
// P2PSessionConnectFail_t
// P2PSessionRequest_t
// SocketStatusCallback_t

// RemoteStorage
// RemoteStorageDownloadUGCResult_t
// RemoteStorageFileShareResult_t
// RemoteStorageFileWriteAsyncComplete_t

// Screenshots
// ScreenshotReady_t
// ScreenshotRequested_t

// UGC
// AddAppDependencyResult_t
// AddUGCDependencyResult_t
// CreateItemResult_t
// DownloadItemResult_t
// GetAppDependenciesResult_t
// GetUserItemVoteResult_t
// ItemInstalled_t
// RemoveAppDependencyResult_t
// RemoveUGCDependencyResult_t
// SetUserItemVoteResult_t
// StartPlaytimeTrackingResult_t
// SteamUGCQueryCompleted_t
// StopPlaytimeTrackingResult_t
// SubmitItemUpdateResult_t
// UserFavoriteItemsListChanged_t

// UnifiedMessages
// SteamUnifiedMessagesSendMethodResult_t

// SteamUser
// ClientGameServerDeny_t
// EncryptedAppTicketResponse_t
// GameWebCallback_t
// GetAuthSessionTicketResponse_t
// IPCFailure_t
// LicensesUpdated_t
// MicroTxnAuthorizationResponse_t
// SteamServerConnectFailure_t

// SteamServersConnected_t // When connected to a server
class SteamPower_e_SteamServersConnected_t : public SteamPowerEvent {
public:
	SteamPower_e_SteamServersConnected_t(SteamServersConnected_t * t) : SteamPowerEvent("SteamServersConnected") {};
	virtual bool tolua(lua_State * L) {
		SteamPowerEvent::tolua(L);
		return 0;
	};
};

void Steam::_SteamServersConnected_t(SteamServersConnected_t* t) {
	auto e = std::make_shared<SteamPower_e_SteamServersConnected_t>(t);
	_event_push(e);
}

// SteamServersDisconnected_t // When disconnected from a server
	class SteamPower_e_SteamServersDisconnected_t : public SteamPowerEvent {
	public:
		SteamPower_e_SteamServersDisconnected_t(SteamServersDisconnected_t * t) : SteamPowerEvent("SteamServersDisconnected") {};

		virtual bool tolua(lua_State * L) {
			SteamPowerEvent::tolua(L);
			return 0;
		};
	};

void Steam::_SteamServersDisconnected_t(SteamServersDisconnected_t* t) {
	auto e = std::make_shared<SteamPower_e_SteamServersDisconnected_t>(t);
	_event_push(e);
}


// StoreAuthURLResponse_t
// ValidateAuthTicketResponse_t

// SteamUserStats
// GlobalAchievementPercentagesReady_t
class SteamPower_e_GlobalAchievementPercentagesReady_t : public SteamPowerEvent {
public:
	uint64 GameID;
	SteamPower_e_GlobalAchievementPercentagesReady_t(GlobalAchievementPercentagesReady_t * t) : SteamPowerEvent("GlobalAchievementPercentagesReady") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;
		GameID = t->m_nGameID;

	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "GameID", GameID);
		return 0;
	};
};

void Steam::_GlobalAchievementPercentagesReady_t(GlobalAchievementPercentagesReady_t *t) {
	auto e = std::make_shared<SteamPower_e_GlobalAchievementPercentagesReady_t>(t);
	_event_push(e);
}

// GlobalStatsReceived_t
class SteamPower_e_GlobalStatsReceived_t : public SteamPowerEvent {
public:
	uint64 GameID;
	SteamPower_e_GlobalStatsReceived_t(GlobalStatsReceived_t * t) : SteamPowerEvent("GlobalStatsReceived") {
		Result_code = t->m_eResult;
		error = EResult_string[t->m_eResult];
		if (t->m_eResult != k_EResultOK) return;
		GameID = t->m_nGameID;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tint_64(L, "GameID", GameID);
		return 0;
	};
};

void Steam::_GlobalStatsReceived_t(GlobalStatsReceived_t *t) {
	auto e = std::make_shared<SteamPower_e_GlobalStatsReceived_t>(t);
	_event_push(e);
}

// LeaderboardFindResult_t // Signal a leaderboard has been loaded or has failed
class SteamPower_e_LeaderboardFindResult_t : public SteamPowerEvent {
public:
	uint64 Leaderboard;
	int    LeaderboardFound;
	SteamPower_e_LeaderboardFindResult_t(LeaderboardFindResult_t * t) : SteamPowerEvent("LeaderboardFindResult") {
		if (t->m_bLeaderboardFound == 0) {
			Result_code = 0;
			error = "No leaderboard fount";
			return;
		}
		LeaderboardFound = t->m_bLeaderboardFound;
		Leaderboard      = t->m_hSteamLeaderboard;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "LeaderboardFound", LeaderboardFound);
		luax_tint_64(L, "Leaderboard", Leaderboard);
		return 0;
	};
};

void Steam::_LeaderboardFindResult_t(LeaderboardFindResult_t *t) {
	auto e = std::make_shared<SteamPower_e_LeaderboardFindResult_t>(t);
	_event_push(e);
}

// LeaderboardScoresDownloaded_t // Signal leaderboard entries are downloaded
class SteamPower_e_LeaderboardScoresDownloaded_t : public SteamPowerEvent {
public:
	#define rankptr std::vector<SteamPower_e_leaderboard_rank>
	rankptr * list;
	SteamPower_e_LeaderboardScoresDownloaded_t(LeaderboardScoresDownloaded_t * t, rankptr * ranklist) : SteamPowerEvent("LeaderboardScoresDownloaded") {
		list = ranklist;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		for (int i = 0; i < list->size(); i++) {
			lua_pushnumber(L, i); lua_newtable(L);
			list->at(i).tolua(L);
			lua_rawset(L, -3);
		}
		return 0;
	};
};

void Steam::_LeaderboardScoresDownloaded_t(LeaderboardScoresDownloaded_t *t) {
	if (t->m_hSteamLeaderboard == leaderboard_handle && getDownloadedLeaderboardEntry(t->m_hSteamLeaderboardEntries, t->m_cEntryCount)) {
		auto e = std::make_shared<SteamPower_e_LeaderboardScoresDownloaded_t>(t, &leaderboard_entries);
		_event_push(e);
	}
}

// LeaderboardScoreUploaded_t
class SteamPower_e_LeaderboardScoreUploaded_t : public SteamPowerEvent {
public:
	int64_t Leaderboard;
	int     Success, Score, ScoreChanged, GlobalRankNew, GlobalRankPrevious;
	SteamPower_e_LeaderboardScoreUploaded_t(LeaderboardScoreUploaded_t * t) : SteamPowerEvent("LeaderboardScoreUploaded") {
		Success            = t->m_bSuccess;
		Score              = t->m_nScore;
		ScoreChanged       = t->m_bScoreChanged;
		GlobalRankNew      = t->m_nGlobalRankNew;
		GlobalRankPrevious = t->m_nGlobalRankPrevious;
		Leaderboard        = t->m_hSteamLeaderboard;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "Success",            Success);
		luax_tnumber(L, "Score",              Score);
		luax_tnumber(L, "ScoreChanged",       ScoreChanged);
		luax_tnumber(L, "GlobalRankNew",      GlobalRankNew);
		luax_tnumber(L, "GlobalRankPrevious", GlobalRankPrevious);
		luax_tint_64(L, "Leaderboard",        Leaderboard);
		return 0;
	};
};

void Steam::_LeaderboardScoreUploaded_t(LeaderboardScoreUploaded_t *t) {
	auto e = std::make_shared<SteamPower_e_LeaderboardScoreUploaded_t>(t);
	_event_push(e);
}

// LeaderboardUGCSet_t
// NumberOfCurrentPlayers_t
// PS3TrophiesInstalled_t
// UserAchievementIconFetched_t
// UserAchievementStored_t
// UserStatsReceived_t
// UserStatsStored_t
// UserStatsUnloaded_t

// Utils
// CheckFileSignature_t
// GamepadTextInputDismissed_t
// IPCountry_t
// LowBatteryPower_t // Signal when battery power is running low, less than 10 minutes left
class SteamPower_e_LowBatteryPower_t : public SteamPowerEvent {
public:
	int MinutesBatteryLeft;
	SteamPower_e_LowBatteryPower_t(LowBatteryPower_t * t) : SteamPowerEvent("LowBatteryPower") {
		MinutesBatteryLeft = t->m_nMinutesBatteryLeft;
	};
	virtual bool tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_newtable(L);
		luax_tnumber(L, "MinutesBatteryLeft", MinutesBatteryLeft);
		return 0;
	};
};

void Steam::_LowBatteryPower_t(LowBatteryPower_t* t) {
	auto e = std::make_shared<SteamPower_e_LowBatteryPower_t>(t);
	_event_push(e);
}

// SteamAPICallCompleted_t
// SteamShutdown_t

// Video
// BroadcastUploadStart_t
// BroadcastUploadStop_t
// GetOPFSettingsResult_t
// GetVideoURLResult_t



// callback-events, steam_event.h/steam_event.cpp











/*
class SteamPower_e_LowBatteryPower_t : public SteamPowerEvent {
	int time_left;
	SteamPower_e_LowBatteryPower_t(LowBatteryPower_t * t) : SteamPowerEvent("LowBatteryPower") {
		time_left = t->m_nMinutesBatteryLeft;
	};
public:
	void tolua(lua_State * L) {
		if (!SteamPowerEvent::tolua(L)) return 1;
		lua_pushnumber(L, time_left);
	};
};

void Steam::_LowBatteryPower_t(LowBatteryPower_t* t) {
	auto e = std::make_shared<SteamPower_e_LowBatteryPower_t>(t);
	_event_push(e);
}
*/



















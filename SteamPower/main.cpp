#include "main.h"
#include "m_applist.h"
#include "m_friends.h"
#include "m_utils.h"

Steam * steam = new Steam();

int SteamPower_event_flush(lua_State *L) {
	if (!steam->_event_flush()) { // Run callbacks
		lua_pushstring(L, "Error in run callbacks");
		lua_error(L);
		return 0;
	}
	lua_pushboolean(L, 1);
	return 1;
}

int SteamPower_event_poll(lua_State *L) {
	if (steam->event_list.size() == 0) {
		lua_pushnil(L);
		return 1;
	}

	steam->event_list.back()->tolua(L);
	steam->debug("eventpool, push event[%s] to lua", steam->event_list.back()->type.c_str());
	steam->event_list.pop_back();

	return lua_gettop(L);

}

int SteamPower_event_clear(lua_State * L) {
	steam->_event_clear();
	lua_pushboolean(L, true);
	return 1;
}

int SteamPower_event_count(lua_State * L) {
	lua_pushinteger(L, steam->_event_count());
	return 1;
}


void SteamPower_event_register(lua_State * L){
	lua_pushstring(L, "event");
	lua_newtable(L);
	luax_tfunction(L, "flush", SteamPower_event_flush);
	luax_tfunction(L, "pool",  SteamPower_event_poll);
	luax_tfunction(L, "clear", SteamPower_event_clear);
	luax_tfunction(L, "count", SteamPower_event_count);
	lua_rawset(L, -3);
}

int SteamPower_debug_set(lua_State * L) {
	steam->setDebug(lua_toboolean(L, 1));
	return 0;
}


extern "C" {
	__declspec(dllexport) int luaopen_SteamPower(lua_State *L) {
		int success = steam->init(true);
		if (success == 1) {
			lua_newtable(L);
			SteamPower_applist_register(L);
			SteamPower_event_register(L);
			SteamPower_friends_register(L);
			SteamPower_utils_register(L);
			luax_tfunction(L, "debug", SteamPower_debug_set);
			return 1;
		}
		else if (success == 2) {
			lua_pushnil(L);
			lua_pushstring(L, "Steam restarted");
			return 2;
		}
		else if (success == 3) {
			lua_pushnil(L);
			lua_pushstring(L, "Steam not initialized");
			return 2;
		}
		lua_pushnil(L);
		lua_pushstring(L, "Unknown error");
		return 2;
	}
}


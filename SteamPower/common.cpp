#include "common.h"
#include <string>

const char * uint64ToChar(uint64 num) {
	return std::to_string(num).c_str();
}

string uint64ToString(uint64 num) {
	return std::to_string(num);
}

uint64 stringToUint64(string str) {
	std::string::size_type sz = 0;
	return std::stoull(str, &sz, 0);
}

void luax_printstack(lua_State * L)
{
	for (int i = 1; i <= lua_gettop(L); i++)
		std::cout << "Stack: " << i << " - " << luaL_typename(L, i) << std::endl;
}

void luax_pushint_64(lua_State * L, uint64 value) {
	lua_pushstring(L, uint64ToChar(value));
}

uint64 luax_checkint_64(lua_State * L, int idx) {
	const char *str = luaL_checkstring(L, idx);
	return stringToUint64(std::string(str));
}

void luax_tint_64(lua_State * L, char * key, uint64 value) {
	lua_pushstring(L, key); lua_pushstring(L, uint64ToChar(value)); lua_rawset(L, -3);
}

void luax_tint_64(lua_State * L, int key, uint64 value) {
	lua_pushnumber(L, key); lua_pushstring(L, uint64ToChar(value)); lua_rawset(L, -3);
}


void luax_tnumber(lua_State * L, char * key, float value) {
	lua_pushstring(L, key); lua_pushnumber(L, value); lua_rawset(L, -3);
}
void luax_tnumber(lua_State * L, int key, float value) {
	lua_pushnumber(L, key); lua_pushnumber(L, value); lua_rawset(L, -3);
}


void luax_tboolean(lua_State * L, char * key, bool value) {
	lua_pushstring(L, key); lua_pushboolean(L, value); lua_rawset(L, -3);
}

void luax_tboolean(lua_State * L, int key, bool value) {
	lua_pushnumber(L, key); lua_pushboolean(L, value); lua_rawset(L, -3);
}


void luax_tfunction(lua_State * L, char * key, lua_CFunction value) {
	lua_pushstring(L, key); lua_pushcfunction(L, value); lua_rawset(L, -3);
}

void luax_tfunction(lua_State * L, int key, lua_CFunction value) {
	lua_pushnumber(L, key); lua_pushcfunction(L, value); lua_rawset(L, -3);
}


void luax_tstring(lua_State * L, char * key, const char * value) {
	lua_pushstring(L, key); lua_pushstring(L, value); lua_rawset(L, -3);
}

void luax_tstring(lua_State * L, int key, const char * value) {
	lua_pushnumber(L, key); lua_pushstring(L, value); lua_rawset(L, -3);
}

std::string luax_toString(lua_State *L, int idx)
{
	size_t len;
	const char *str = lua_tolstring(L, idx, &len);
	return std::string(str, len);
}

std::string luax_checkString(lua_State *L, int idx)
{
	size_t len;
	const char *str = luaL_checklstring(L, idx, &len);
	return std::string(str, len);
}

void luax_pushString(lua_State *L, const std::string &str) {
	lua_pushlstring(L, str.data(), str.size());
}

void luax_tString(lua_State *L, char * key, const std::string &str) {
	lua_pushstring(L, key);  lua_pushlstring(L, str.data(), str.size()); lua_rawset(L, -3);
}

void luax_tString(lua_State *L, int key, const std::string &str) {
	lua_pushnumber(L, key);  lua_pushlstring(L, str.data(), str.size()); lua_rawset(L, -3);
}
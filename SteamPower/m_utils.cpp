#include "m_utils.h"
#include "main.h"
#include <string>

extern Steam * steam;

int SteamPower_utils_load_image_by_id(lua_State * L) {

	int id = (int)luaL_checknumber(L, 1);
	uint32 width, height;
	bool success = SteamUtils()->GetImageSize(id, &width, &height);

	if (!success) {
		lua_pushnil(L);
		lua_pushstring(L, "Failed to load image size, bad id");
		return 2;
	}
	steam->debug("Utils: Load image by id [%d]: %dx%d", id, width, height);

	int bsize = 4 * width * height;
	uint8 * iBuffer = new uint8[bsize];

	success = SteamUtils()->GetImageRGBA(id, iBuffer, bsize);

	if (!success) {
		lua_pushnil(L);
		lua_pushstring(L, "Failed to load image RGBA");
		delete iBuffer;
		return 2;
	}

	std::string output(bsize, 0);
	for (size_t i = 0; i < bsize; ++i)
		output[i] = static_cast<char>(iBuffer[i]);
	luax_pushString(L, output);
	lua_pushnumber(L, width);
	lua_pushnumber(L, height);
	delete iBuffer;
	return 3;
}

/* // Warning messages?
extern auto list = warning_list;
int SteamPower_utils_get_warning_messages(lua_State * L) {
	lua_newtable(L);
	for (int i = 0; warning_list.size(); i++) {
		lua_pushnumber(L, i + 1);
		lua_newtable(L);
		warning_list.at(i).tolua(L);
		lua_rawset(L, -3);
	}
	warning_list.clear();
	return 1;
}*/

void SteamPower_utils_register(lua_State * L) {
	lua_pushstring(L, "utils");
	lua_newtable(L);
	luax_tfunction(L, "LoadImageByID", SteamPower_utils_load_image_by_id); // CUSTOM FUNCTION, RETURNS string IMGDATA, number WIDTH, number HEIGHT of image
	//luax_tfunction(L, "getDebugInfo", SteamPower_utils_get_warning_messages);
	lua_rawset(L, -3);
}

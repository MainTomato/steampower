#include "main.h"
#include "m_friends.h"
#include <string>
extern Steam * steam;

static const char * EPersonaState_String[] = {
	"Offline",
	"Online",
	"Busy",
	"Away",
	"Snooze",
	"LookingToTrade",
	"LookingToPlay",
	"Max",
};

static const char * EUserRestriction_String[] = {
	"None",
	"Unknown",
	"AnyChat",
	"VoiceChat",
	"GroupChat",
	"Rating",
	"GameInvites",
	"Trading",
};


// SteamPower_friends_ActivateGameOverlay
// SteamPower_friends_ActivateGameOverlayInviteDialog
// SteamPower_friends_ActivateGameOverlayToStore
// SteamPower_friends_ActivateGameOverlayToUser
// SteamPower_friends_ActivateGameOverlayToWebPage
// SteamPower_friends_ClearRichPresence
// SteamPower_friends_CloseClanChatWindowInSteam
// SteamPower_friends_DownloadClanActivityCounts
// SteamPower_friends_EnumerateFollowingList
// SteamPower_friends_GetChatMemberByIndex
// SteamPower_friends_GetClanActivityCounts
// SteamPower_friends_GetClanByIndex
// SteamPower_friends_GetClanChatMemberCount
// SteamPower_friends_GetClanChatMessage
// SteamPower_friends_GetClanCount
// SteamPower_friends_GetClanName
// SteamPower_friends_GetClanOfficerByIndex
// SteamPower_friends_GetClanOfficerCount
// SteamPower_friends_GetClanOwner
// SteamPower_friends_GetClanTag
// SteamPower_friends_GetCoplayFriend
// SteamPower_friends_GetCoplayFriendCount
// SteamPower_friends_GetFollowerCount
// SteamPower_friends_GetFriendByIndex
int SteamPower_friends_GetFriendByIndex(lua_State * L) {
	int i = luaL_checknumber(L, 1);
	uint64 SteamID = SteamFriends()->GetFriendByIndex(i - 1, k_EFriendFlagImmediate).GetStaticAccountKey();
	steam->debug("Friends: friend id by index [%d]: [%s]", i, uint64ToChar(SteamID));
	luax_pushint_64(L, SteamID);
	return 1;
}

// SteamPower_friends_GetFriendCoplayGame
// SteamPower_friends_GetFriendCoplayTime
// SteamPower_friends_GetFriendsCount
int SteamPower_friends_GetFriendsCount(lua_State * L) {
	int n = SteamFriends()->GetFriendCount(k_EFriendFlagImmediate);
	steam->debug("Friends: get friend count: [%d]", n);
	lua_pushnumber(L, n);
	return 1;
}

// SteamPower_friends_GetFriendCountFromSource
// SteamPower_friends_GetFriendFromSourceByIndex
// SteamPower_friends_GetFriendGamePlayed
int SteamPower_friends_GetFriendGamePlayed(lua_State * L) {
	uint64 SteamID        = luax_checkint_64(L, 1);
	FriendGameInfo_t t;
	bool info = SteamFriends()->GetFriendGamePlayed(SteamID, &t);

	lua_pushboolean(L, info);
	lua_newtable(L);
	luax_tnumber(L, "AppID",     t.m_gameID.AppID());
	luax_tnumber(L, "GameIP",    t.m_unGameIP);
	luax_tnumber(L, "GamePort",  t.m_usGamePort);
	luax_tnumber(L, "QueryPort", t.m_usQueryPort);
	luax_tint_64(L, "IDLobby",   t.m_steamIDLobby.GetStaticAccountKey());

	if (t.m_unGameIP) {
		uint8 ip_array[4] = { 0, 0, 0, 0 };
		char * ip_addr = new char[16];
		for (int i = 0; i < 4; i++)
			ip_array[i] = t.m_unGameIP >> (i * 8);
		sprintf(ip_addr, "%u.%u.%u.%u", ip_array[3], ip_array[2], ip_array[1], ip_array[0]);
		luax_tString(L, "GameIPString", ip_addr);
	}

	steam->debug("Friends: friend by id is played? [%s]: [%d]", uint64ToChar(SteamID), info);
	return 2;
}

// SteamPower_friends_GetFriendMessage
// SteamPower_friends_GetFriendPersonaName
int SteamPower_friends_GetFriendPersonaName(lua_State * L) {
	uint64 SteamID = luax_checkint_64(L, 1);

	const char * name = SteamFriends()->GetFriendPersonaName(SteamID);
	lua_pushstring(L, name);
	steam->debug("Friends: friend name by id [%s]: [%s]", uint64ToChar(SteamID), name);
	return 1;
}

// SteamPower_friends_GetFriendPersonaNameHistory
// SteamPower_friends_GetFriendPersonaState
int SteamPower_friends_GetFriendPersonaState(lua_State * L) {
	uint64 SteamID = luax_checkint_64(L, 1);
	uint32 state   = SteamFriends()->GetFriendPersonaState(SteamID);
	string status  = EPersonaState_String[state];
	luax_pushString(L, status);
	lua_pushnumber(L, state);
	steam->debug("Friends: friend state by id [%s]: [%d][%s]", uint64ToChar(SteamID), state, status.c_str());
	return 2;
}

// SteamPower_friends_GetFriendRelationship
// SteamPower_friends_GetFriendRichPresence
// SteamPower_friends_GetFriendRichPresenceKeyByIndex
// SteamPower_friends_GetFriendRichPresenceKeyCount
// SteamPower_friends_GetFriendsGroupCount
// SteamPower_friends_GetFriendsGroupIDByIndex
// SteamPower_friends_GetFriendsGroupMembersCount
// SteamPower_friends_GetFriendsGroupMembersList
// SteamPower_friends_GetFriendsGroupMembersList
// SteamPower_friends_GetFriendSteamLevel
int SteamPower_friends_GetFriendSteamLevel(lua_State * L) {
	uint64 SteamID = luax_checkint_64(L, 1);
	int level = SteamFriends()->GetFriendSteamLevel(SteamID);
	steam->debug("Friends: friend steam level by id [%s]: [%d]", uint64ToChar(SteamID), level);
	lua_pushnumber(L, level);
	return 1;
}

// SteamPower_friends_GetFriendAvatarID
int SteamPower_friends_GetFriendAvatarID(lua_State * L) {
	uint64 SteamID = luax_checkint_64(L, 1);
	int size = 2;    // default
	if (lua_isnumber(L, 2)) {
		int usersize = luaL_checkinteger(L, 2);
		if (usersize > 0 && usersize < 4) size = usersize;
	}

	int img_id;
	if      (size == 1)
		img_id = SteamFriends()->GetSmallFriendAvatar(SteamID);
	else if (size == 2)
		img_id = SteamFriends()->GetMediumFriendAvatar(SteamID);
	else
		img_id = SteamFriends()->GetLargeFriendAvatar(SteamID);

	steam->debug("Friends: friend avatar id by size [%d]: [%d]", size, img_id);
	if (img_id > 0)
		lua_pushnumber(L, img_id);
	else
		lua_pushboolean(L, false);
	return 1;
}

// SteamPower_friends_GetLargeFriendAvatar
// SteamPower_friends_GetMediumFriendAvatar
// SteamPower_friends_GetPersonaName
// SteamPower_friends_GetPersonaState
// SteamPower_friends_GetPlayerNickname
// SteamPower_friends_GetSmallFriendAvatar
// SteamPower_friends_GetUserRestrictions
int SteamPower_friends_GetUserRestrictions(lua_State * L) {
	uint64 SteamID = luax_checkint_64(L, 1);
	if (SteamID == 0) {
		lua_pushstring(L, "arg#1: String id expected");
		lua_error(L);
	}
	int state = SteamFriends()->GetUserRestrictions();
	lua_pushstring(L, EUserRestriction_String[state]);
	lua_pushnumber(L, state);
	steam->debug("Friends: friend state by id [%s]: [%s]", uint64ToChar(SteamID), state);
	return 1;
}

// SteamPower_friends_HasFriend
// SteamPower_friends_InviteUserToGame
// SteamPower_friends_IsClanChatAdmin
// SteamPower_friends_IsClanChatWindowOpenInSteam
// SteamPower_friends_IsFollowing
// SteamPower_friends_IsUserInSource
// SteamPower_friends_JoinClanChatRoom
// SteamPower_friends_LeaveClanChatRoom
// SteamPower_friends_OpenClanChatWindowInSteam
// SteamPower_friends_ReplyToFriendMessage
// SteamPower_friends_RequestClanOfficerList
// SteamPower_friends_RequestFriendRichPresence
// SteamPower_friends_RequestUserInformation
// SteamPower_friends_SendClanChatMessage
// SteamPower_friends_SetInGameVoiceSpeaking
// SteamPower_friends_SetListenForFriendsMessages
// SteamPower_friends_SetPersonaName
// SteamPower_friends_SetPlayedWith
// SteamPower_friends_SetRichPresence


void SteamPower_friends_register(lua_State * L) {
	lua_pushstring(L, "friends");
	lua_newtable(L);
	//luax_tfunction(L, "ActivateGameOverlay",              SteamPower_friends_ActivateGameOverlay);
	//luax_tfunction(L, "ActivateGameOverlayInviteDialog",  SteamPower_friends_ActivateGameOverlayInviteDialog);
	//luax_tfunction(L, "ActivateGameOverlayToStore",       SteamPower_friends_ActivateGameOverlayToStore);
	//luax_tfunction(L, "ActivateGameOverlayToUser",        SteamPower_friends_ActivateGameOverlayToUser);
	//luax_tfunction(L, "ActivateGameOverlayToWebPage",     SteamPower_friends_ActivateGameOverlayToWebPage);
	//luax_tfunction(L, "ClearRichPresence",                SteamPower_friends_ClearRichPresence);
	//luax_tfunction(L, "CloseClanChatWindowInSteam",       SteamPower_friends_CloseClanChatWindowInSteam);
	//luax_tfunction(L, "DownloadClanActivityCounts",       SteamPower_friends_DownloadClanActivityCounts);
	//luax_tfunction(L, "EnumerateFollowingList",           SteamPower_friends_EnumerateFollowingList);
	//luax_tfunction(L, "GetChatMemberByIndex",             SteamPower_friends_GetChatMemberByIndex);
	//luax_tfunction(L, "GetClanActivityCounts",            SteamPower_friends_GetClanActivityCounts);
	//luax_tfunction(L, "GetClanByIndex",                   SteamPower_friends_GetClanByIndex);
	//luax_tfunction(L, "GetClanChatMemberCount",           SteamPower_friends_GetClanChatMemberCount);
	//luax_tfunction(L, "GetClanChatMessage",               SteamPower_friends_GetClanChatMessage);
	//luax_tfunction(L, "GetClanCount",                     SteamPower_friends_GetClanCount);
	//luax_tfunction(L, "GetClanName",                      SteamPower_friends_GetClanName);
	//luax_tfunction(L, "GetClanOfficerByIndex",            SteamPower_friends_GetClanOfficerByIndex);
	//luax_tfunction(L, "GetClanOfficerCount",              SteamPower_friends_GetClanOfficerCount);
	//luax_tfunction(L, "GetClanOwner",                     SteamPower_friends_GetClanOwner);
	//luax_tfunction(L, "GetClanTag",                       SteamPower_friends_GetClanTag);
	//luax_tfunction(L, "GetCoplayFriend",                  SteamPower_friends_GetCoplayFriend);
	//luax_tfunction(L, "GetCoplayFriendCount",             SteamPower_friends_GetCoplayFriendCount);
	//luax_tfunction(L, "GetFollowerCount",                 SteamPower_friends_GetFollowerCount);
	luax_tfunction(L, "GetFriendByIndex",                   SteamPower_friends_GetFriendByIndex);
	//luax_tfunction(L, "GetFriendCoplayGame",              SteamPower_friends_GetFriendCoplayGame);
	//luax_tfunction(L, "GetFriendCoplayTime",              SteamPower_friends_GetFriendCoplayTime);
	luax_tfunction(L, "GetFriendsCount",                    SteamPower_friends_GetFriendsCount);
	//luax_tfunction(L, "GetFriendCountFromSource",         SteamPower_friends_GetFriendCountFromSource);
	//luax_tfunction(L, "GetFriendFromSourceByIndex",       SteamPower_friends_GetFriendFromSourceByIndex);
	luax_tfunction(L, "GetFriendGamePlayed",              SteamPower_friends_GetFriendGamePlayed);
	//luax_tfunction(L, "GetFriendMessage",                 SteamPower_friends_GetFriendMessage);
	luax_tfunction(L, "GetFriendPersonaName",               SteamPower_friends_GetFriendPersonaName);
	//luax_tfunction(L, "GetFriendPersonaNameHistory",      SteamPower_friends_GetFriendPersonaNameHistory);
	luax_tfunction(L, "GetFriendPersonaState",              SteamPower_friends_GetFriendPersonaState);
	//luax_tfunction(L, "GetFriendRelationship",            SteamPower_friends_GetFriendRelationship);
	//luax_tfunction(L, "GetFriendRichPresence",            SteamPower_friends_GetFriendRichPresence);
	//luax_tfunction(L, "GetFriendRichPresenceKeyByIndex",  SteamPower_friends_GetFriendRichPresenceKeyByIndex);
	//luax_tfunction(L, "GetFriendRichPresenceKeyCount",    SteamPower_friends_GetFriendRichPresenceKeyCount);
	//luax_tfunction(L, "GetFriendsGroupCount",             SteamPower_friends_GetFriendsGroupCount);
	//luax_tfunction(L, "GetFriendsGroupIDByIndex",         SteamPower_friends_GetFriendsGroupIDByIndex);
	//luax_tfunction(L, "GetFriendsGroupMembersCount",      SteamPower_friends_GetFriendsGroupMembersCount);
	//luax_tfunction(L, "GetFriendsGroupMembersList",       SteamPower_friends_GetFriendsGroupMembersList);
	//luax_tfunction(L, "GetFriendsGroupMembersList",       SteamPower_friends_GetFriendsGroupMembersList);
	luax_tfunction(L, "GetFriendSteamLevel",                SteamPower_friends_GetFriendSteamLevel);
	luax_tfunction(L, "GetFriendAvatarID",                  SteamPower_friends_GetFriendAvatarID); // CUSTOM FUNCTION, (string SteamID, int Size[1/2/3])
	//luax_tfunction(L, "GetLargeFriendAvatar",             SteamPower_friends_GetLargeFriendAvatar);
	//luax_tfunction(L, "GetMediumFriendAvatar",            SteamPower_friends_GetMediumFriendAvatar);
	//luax_tfunction(L, "GetPersonaName",                   SteamPower_friends_GetPersonaName);
	//luax_tfunction(L, "GetPersonaState",                  SteamPower_friends_GetPersonaState);
	//luax_tfunction(L, "GetPlayerNickname",                SteamPower_friends_GetPlayerNickname);
	//luax_tfunction(L, "GetSmallFriendAvatar",             SteamPower_friends_GetSmallFriendAvatar);
	luax_tfunction(L, "GetUserRestrictions",              SteamPower_friends_GetUserRestrictions);
	//luax_tfunction(L, "HasFriend",                        SteamPower_friends_HasFriend);
	//luax_tfunction(L, "InviteUserToGame",                 SteamPower_friends_InviteUserToGame);
	//luax_tfunction(L, "IsClanChatAdmin",                  SteamPower_friends_IsClanChatAdmin);
	//luax_tfunction(L, "IsClanChatWindowOpenInSteam",      SteamPower_friends_IsClanChatWindowOpenInSteam);
	//luax_tfunction(L, "IsFollowing",                      SteamPower_friends_IsFollowing);
	//luax_tfunction(L, "IsUserInSource",                   SteamPower_friends_IsUserInSource);
	//luax_tfunction(L, "JoinClanChatRoom",                 SteamPower_friends_JoinClanChatRoom);
	//luax_tfunction(L, "LeaveClanChatRoom",                SteamPower_friends_LeaveClanChatRoom);
	//luax_tfunction(L, "OpenClanChatWindowInSteam",        SteamPower_friends_OpenClanChatWindowInSteam);
	//luax_tfunction(L, "ReplyToFriendMessage",             SteamPower_friends_ReplyToFriendMessage);
	//luax_tfunction(L, "RequestClanOfficerList",           SteamPower_friends_RequestClanOfficerList);
	//luax_tfunction(L, "RequestFriendRichPresence",        SteamPower_friends_RequestFriendRichPresence);
	//luax_tfunction(L, "RequestUserInformation",           SteamPower_friends_RequestUserInformation);
	//luax_tfunction(L, "SendClanChatMessage",              SteamPower_friends_SendClanChatMessage);
	//luax_tfunction(L, "SetInGameVoiceSpeaking",           SteamPower_friends_SetInGameVoiceSpeaking);
	//luax_tfunction(L, "SetListenForFriendsMessages",      SteamPower_friends_SetListenForFriendsMessages);
	//luax_tfunction(L, "SetPersonaName",                   SteamPower_friends_SetPersonaName);
	//luax_tfunction(L, "SetPlayedWith",                    SteamPower_friends_SetPlayedWith);
	//luax_tfunction(L, "SetRichPresence",                  SteamPower_friends_SetRichPresence);
	lua_rawset(L, -3);
}

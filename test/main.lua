-- check path to dll, or put it onto love-dist directory
--package.cpath = package.cpath..[[;C:\drive\LOVE\__love-dist\?.dll]]
package.cpath = [[D:\drive\GitProj\SteamPower\Release\?.dll;]]..package.cpath

steam, err = require'SteamPower'
print('STEAM:', steam, 'error:', err)
serpent = require'serpent'

steam.debug(true)

print(serpent.block(steam))

function love.update(dt)
	steam.event.flush()
	local succ, name, res = steam.event.pool()
	while name do
		local t = {
			succ = succ,
			[(succ and 'Event' or 'Error')] = name,
			result = res,
		}
		local id = type(res) == 'table' and res.SteamID or type(res) == 'table' and res.IDFriend
		if id then
			t.name = steam.friends.GetFriendPersonaName(id)
			t.lvl = steam.friends.GetFriendSteamLevel(id)
		end
		print('E', serpent.block(t)
		)
		
		succ, name, res, _ = steam.event.pool()
	end
end

local friends = {current_block = 1}

function friends:load()
	for i = 1, steam.friends.GetFriendsCount() do
		local id    = steam.friends.GetFriendByIndex(i)
		local imgid = steam.friends.GetFriendAvatarID(id, 2)
		local imgdata, w, h = steam.utils.LoadImageByID(imgid)
		imgdata = love.image.newImageData(w, h, imgdata)
		
		local o = {}
		o.id     = id
		o.img    = love.graphics.newImage(imgdata)
		o.name   = steam.friends.GetFriendPersonaName(id)
		o.level  = steam.friends.GetFriendSteamLevel(id)
		o.status = steam.friends.GetFriendPersonaState(id)
		o.game   = '--'
		
		local inGame, GameInfo = steam.friends.GetFriendGamePlayed(id)
		if inGame then
			o.game  = steam.applist.GetAppName(GameInfo.AppID)
			o.gameinfo = GameInfo
		end
		
		print(serpent.block(o))
		
		if not self[self.current_block] then
			self[self.current_block] = {}
		end
		if #self[self.current_block] > 3 then
			self.current_block = self.current_block + 1
		end
		if not self[self.current_block] then
			self[self.current_block] = {}
		end
		
		table.insert(self[self.current_block], o)
	end
	self.current_block = 1
end

function friends:draw()
	local gr = love.graphics
	gr.translate(.5, .5)
	gr.print('Current friends block: '..self.current_block..'/'..#self)
  local cblock = self[self.current_block]
	for i = 1, #cblock do
	  local v = cblock[i]
		local x, y = 20, i * 110
		gr.setColor(100, 100, 100); gr.rectangle('line', x, y, 600, 110); 		gr.setColor(255, 255, 255)
		gr.draw (v.img,  x + 12, y + 22)
		gr.print('Name:   '..v.name,   x + 84,  y + 10)
		gr.print('Level:  '..v.level,  x + 84,  y + 35)
		gr.print('Status: '..v.status, x + 284, y + 35)
		gr.print('Game:   '..v.game,   x + 84,  y + 60)
	end
end

local function loop(v, min, max)
	return v > max and min or v < min and max or v
end

function friends:keypressed(key)
	if key == 'left'  then self.current_block = loop(self.current_block - 1, 1, #self) end
	if key == 'right' then self.current_block = loop(self.current_block + 1, 1, #self) end
end

function love.load()
	love.graphics.setNewFont('PTMono.ttf', 24)
	love.graphics.setDefaultFilter'nearest'
	friends:load()
end

function love.keypressed(key)
	friends:keypressed(key)
	if key == '1' then
		steam.debug(true)
	elseif key == '2' then
		steam.debug(false)
	end
end

function love.draw()
	friends:draw()
end
